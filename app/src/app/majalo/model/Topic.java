package app.majalo.model;

public class Topic {

    private int id;
    private String title;
    private int order;
    private String thumb_url;

    public Topic() {}

    public Topic(int id,String title,int order,String thumb_url) {
        this.setId(id);
        this.setTitle(title);
        this.setOrder(order);
        this.setThumb_url(thumb_url);
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getOrder() {
        return order;
    }

    public String getThumb_url() {
        return thumb_url;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public void setThumb_url(String thumb_url) {
        this.thumb_url = thumb_url;
    }
}