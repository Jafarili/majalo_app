package app.majalo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Post {

    @SerializedName("ID")
    @Expose
    private Integer iD;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("PublishDate")
    @Expose
    private String publishDate;
    @SerializedName("PublishTime")
    @Expose
    private String publishTime;
    @SerializedName("Body")
    @Expose
    private String body;
    @SerializedName("Url")
    @Expose
    private String url;
    @SerializedName("Astonished")
    @Expose
    private String astonished;
    @SerializedName("Pleased")
    @Expose
    private String pleased;
    @SerializedName("Indifferent")
    @Expose
    private String indifferent;
    @SerializedName("Worried")
    @Expose
    private String worried;
    @SerializedName("Sorry")
    @Expose
    private String sorry;
    @SerializedName("Category")
    @Expose
    private String category;
    @SerializedName("Thumb_url1")
    @Expose
    private String thumbUrl1;
    @SerializedName("CommentNo")
    @Expose
    private String commentNo;
    @SerializedName("ImageNo")
    @Expose
    private String imageNo;
    @SerializedName("IsBreakingNews")
    @Expose
    private String isBreakingNews;
    @SerializedName("VisitNo")
    @Expose
    private String visitNo;
    @SerializedName("IsOnTop")
    @Expose
    private String isOnTop;
    @SerializedName("IsHot")
    @Expose
    private String isHot;
    @SerializedName("IsImportant")
    @Expose
    private String isImportant;
    @SerializedName("Resource")
    @Expose
    private String resource;
    @SerializedName("ResTitle")
    @Expose
    private String resTitle;
    @SerializedName("IsVideoStream")
    @Expose
    private Integer isVideoStream;
    @SerializedName("IsVocalStream")
    @Expose
    private Integer isVocalStream;
    @SerializedName("StreamUrl")
    @Expose
    private String streamUrl;
    @SerializedName("IsAdver")
    @Expose
    private String isAdver;
    @SerializedName("CName")
    @Expose
    private Object cName;
    @SerializedName("ImgUrlProfile")
    @Expose
    private Object imgUrlProfile;
    @SerializedName("CatTitle")
    @Expose
    private String catTitle;
    @SerializedName("MMSize")
    @Expose
    private String mMSize;
    @SerializedName("MMTime")
    @Expose
    private String mMTime;

    public Integer getID() {
        return iD;
    }

    public void setID(Integer iD) {
        this.iD = iD;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public String getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(String publishTime) {
        this.publishTime = publishTime;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAstonished() {
        return astonished;
    }

    public void setAstonished(String astonished) {
        this.astonished = astonished;
    }

    public String getPleased() {
        return pleased;
    }

    public void setPleased(String pleased) {
        this.pleased = pleased;
    }

    public String getIndifferent() {
        return indifferent;
    }

    public void setIndifferent(String indifferent) {
        this.indifferent = indifferent;
    }

    public String getWorried() {
        return worried;
    }

    public void setWorried(String worried) {
        this.worried = worried;
    }

    public String getSorry() {
        return sorry;
    }

    public void setSorry(String sorry) {
        this.sorry = sorry;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getThumbUrl1() {
        return thumbUrl1;
    }

    public void setThumbUrl1(String thumbUrl1) {
        this.thumbUrl1 = thumbUrl1;
    }

    public String getCommentNo() {
        return commentNo;
    }

    public void setCommentNo(String commentNo) {
        this.commentNo = commentNo;
    }

    public String getImageNo() {
        return imageNo;
    }

    public void setImageNo(String imageNo) {
        this.imageNo = imageNo;
    }

    public String getIsBreakingNews() {
        return isBreakingNews;
    }

    public void setIsBreakingNews(String isBreakingNews) {
        this.isBreakingNews = isBreakingNews;
    }

    public String getVisitNo() {
        return visitNo;
    }

    public void setVisitNo(String visitNo) {
        this.visitNo = visitNo;
    }

    public String getIsOnTop() {
        return isOnTop;
    }

    public void setIsOnTop(String isOnTop) {
        this.isOnTop = isOnTop;
    }

    public String getIsHot() {
        return isHot;
    }

    public void setIsHot(String isHot) {
        this.isHot = isHot;
    }

    public String getIsImportant() {
        return isImportant;
    }

    public void setIsImportant(String isImportant) {
        this.isImportant = isImportant;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getResTitle() {
        return resTitle;
    }

    public void setResTitle(String resTitle) {
        this.resTitle = resTitle;
    }

    public Integer getIsVideoStream() {
        return isVideoStream;
    }

    public void setIsVideoStream(Integer isVideoStream) {
        this.isVideoStream = isVideoStream;
    }

    public Integer getIsVocalStream() {
        return isVocalStream;
    }

    public void setIsVocalStream(Integer isVocalStream) {
        this.isVocalStream = isVocalStream;
    }

    public String getStreamUrl() {
        return streamUrl;
    }

    public void setStreamUrl(String streamUrl) {
        this.streamUrl = streamUrl;
    }

    public String getIsAdver() {
        return isAdver;
    }

    public void setIsAdver(String isAdver) {
        this.isAdver = isAdver;
    }

    public Object getCName() {
        return cName;
    }

    public void setCName(Object cName) {
        this.cName = cName;
    }

    public Object getImgUrlProfile() {
        return imgUrlProfile;
    }

    public void setImgUrlProfile(Object imgUrlProfile) {
        this.imgUrlProfile = imgUrlProfile;
    }

    public String getCatTitle() {
        return catTitle;
    }

    public void setCatTitle(String catTitle) {
        this.catTitle = catTitle;
    }

    public String getMMSize() {
        return mMSize;
    }

    public void setMMSize(String mMSize) {
        this.mMSize = mMSize;
    }

    public String getMMTime() {
        return mMTime;
    }

    public void setMMTime(String mMTime) {
        this.mMTime = mMTime;
    }

}