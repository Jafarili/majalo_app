package app.majalo;

import com.google.gson.JsonObject;

import java.util.List;

import app.majalo.model.Topic;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface ApiInterface {
    @GET("getTopicList.php")
    Call<List<Topic>> getTopicList();

    @POST("GetNewsList")
    Call<JsonObject> getTopicPosts(@Body GetTopicPostsRequest body);

    @POST("GetNewsDetail")
    Call<JsonObject> getPostDetails(@Body GetPostsDetailsRequest body);

    @POST("GetNewsGallery")
    Call<JsonObject> getPostGallery(@Body GetPostsDetailsRequest body);

    @FormUrlEncoded
    @POST("submitFeedback.php")
    Call<JsonObject> submitFeedback(@Field("content") String content,@Field("phone") String phone);

}
