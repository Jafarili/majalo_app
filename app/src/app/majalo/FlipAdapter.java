package app.majalo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.DialogPlusBuilder;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import app.majalo.activity.PostViewActivity;
import app.majalo.activity.TopicActivity;
import app.majalo.model.Post;
import app.majalo.model.Topic;
import se.emilsjolander.flipview.FlipView;

public class FlipAdapter extends BaseAdapter {
    private Activity context;
    private List<Post> postList = new ArrayList<Post>();
    private List<Integer> flip_count = new ArrayList<Integer>();
    private List<Integer> flip_type = new ArrayList<Integer>();
    private List<Integer> flip_data = new ArrayList<Integer>();
    private Tracker mTracker;
    private FlipView mFlipView;
    public DialogPlusBuilder dialog;
    public double screenInches;
    private Topic topic;

    private LayoutInflater inflater;

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public FlipAdapter(FlipView flipView , Activity context, double screenInches/*, List<Post> objects, List<Integer> FlipCount*/) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.mFlipView = flipView;
        this.dialog = DialogPlus.newDialog(context);
        this.screenInches = screenInches;
        mTracker = ((Application) context.getApplication()).getDefaultTracker();
    }

    @Override
    public int getCount() {
        return flip_count.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public int getItemViewType(int position) {
        return flip_type.get(position)-1;
    }

    @Override
    public int getViewTypeCount() {
        return 7;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        int type = flip_type.get(position);
        int data = flip_data.get(position);

        int start = 0;
        for (int s=0;s < position;s++)
            start = start + flip_count.get(s);

        if (type == 1){
            final Post st_data;
            final Post nd_data;
            final Post rd_data;

            if (postList.get(start).getThumbUrl1() != null) {
                st_data = postList.get(start);
                if (data == 1) {
                    nd_data = postList.get(start + 1);
                    rd_data = postList.get(start + 2);
                } else {
                    nd_data = postList.get(start + 2);
                    rd_data = postList.get(start + 1);
                }
            }else if (postList.get(start+1).getThumbUrl1() != null) {
                st_data = postList.get(start + 1);
                if (data == 1) {
                    nd_data = postList.get(start);
                    rd_data = postList.get(start + 2);
                } else {
                    nd_data = postList.get(start + 2);
                    rd_data = postList.get(start);
                }
            }else {
                st_data = postList.get(start + 2);
                if (data == 1) {
                    nd_data = postList.get(start + 1);
                    rd_data = postList.get(start);
                } else {
                    nd_data = postList.get(start);
                    rd_data = postList.get(start + 1);
                }
            }

            if(convertView == null){
                convertView = inflater.inflate(R.layout.feed_template_1, parent, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            }else{
                holder = (ViewHolder) convertView.getTag();
            }

            holder.st_title.setText(st_data.getTitle());
            holder.st_holder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("FlipAdapter")
                            .setAction("Post Open")
                            .setLabel("st_st")
                            .build());
                    Intent intent = new Intent(context, PostViewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("object", new Gson().toJson(st_data));
                    //bundle.putString("id", st_data.getId());
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                }
            });
            holder.st_holder.setLongClickable(true);

            if (st_data.getResTitle() != null && st_data.getResTitle().trim().length() != 0) {
                holder.st_site.setVisibility(View.VISIBLE);
                holder.st_site.setText(st_data.getResTitle());
            }else
                holder.st_site.setVisibility(View.GONE);

            if (st_data.getThumbUrl1() != null) {
                holder.st_photo.setVisibility(View.VISIBLE);
                Picasso.with(context).load(st_data.getThumbUrl1()).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.trending_gradient_shape).fit().into(holder.st_photo, new Callback() {
                    @Override
                    public void onSuccess() {}
                    @Override
                    public void onError() {
                        Picasso.with(context).load(st_data.getThumbUrl1()).placeholder(R.drawable.trending_gradient_shape).fit().into(holder.st_photo);
                    }
                });
            }else
                holder.st_photo.setVisibility(View.GONE);

            /*if (st_data.getTopic() != null && st_data.getTopic().has("name") && st_data.getTopic().get("name") != null){
                GradientDrawable gd = new GradientDrawable();
                if (st_data.getTopic().has("color") && st_data.getTopic().get("color") != null)
                    gd.setColor(Color.parseColor("#99"+st_data.getTopic().get("color").getAsString().substring(1)));
                else
                    gd.setColor(Color.parseColor("#99ff0000"));
                gd.setCornerRadius(1);
                holder.st_topic.setBackgroundDrawable(gd);
                holder.st_topic.setPadding(8,2,8,2);
                holder.st_topic.setText(st_data.getTopic().get("name").getAsString());
                holder.st_topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("FlipAdapter")
                                .setAction("Post Topic Click")
                                .build());
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("_id", st_data.getTopic().get("_id").getAsJsonObject().get("$id").getAsString());
                        bundle.putString("name", st_data.getTopic().get("name").getAsString());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.st_topic_holder.setVisibility(View.VISIBLE);
            }else {
                holder.st_topic_holder.setVisibility(View.GONE);
            }*/

            if (topic != null){
                GradientDrawable gd = new GradientDrawable();
                gd.setColor(Color.parseColor("#99ff0000"));
                gd.setCornerRadius(1);
                holder.st_topic.setBackgroundDrawable(gd);
                holder.st_topic.setPadding(8,2,8,2);
                holder.st_topic.setText(topic.getTitle());
                holder.st_topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("FlipAdapter")
                                .setAction("Post Topic Click")
                                .build());
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("id", topic.getId()+"");
                        bundle.putString("title", topic.getTitle());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.st_topic_holder.setVisibility(View.VISIBLE);
            }else {
                holder.st_topic_holder.setVisibility(View.GONE);
            }

            /*holder.st_add_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    ((Application) context.getApplication()).share_to_board(context,st_data);
                }
            });*/

            /*try {
                ((Application) context.getApplication()).mViews.remove(st_data.getId());
            }catch (UnsupportedOperationException w){}*/

            /*holder.st_share_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, st_data.getResTitle() + ":\n\n"+st_data.getTitle() + "\n\nhttp://bekhan.me/s/"+st_data.getId()+"\n\n"+st_data.getIfRawPost().toString().substring(0,st_data.getIfRawPost().substring(0, Math.min(st_data.getIfRawPost().length(), 400)).lastIndexOf(' ')+1)+"..."+"\n\n\n"+"منتشر شده در مجلو");
                    sendIntent.setType("text/plain");
                    context.startActivity(sendIntent);
                }
            });*/

            holder.nd_holder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("FlipAdapter")
                            .setAction("Post Open")
                            .setLabel("st_nd")
                            .build());
                    Intent intent = new Intent(context, PostViewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("object", new Gson().toJson(nd_data));
                    //bundle.putString("id", st_data.getId());
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                }
            });
            holder.nd_holder.setLongClickable(true);
            holder.nd_title.setText(nd_data.getTitle()/*.substring(0, Math.min(nd_data.getTitle().length(), 100))*/);
            /*if (nd_data.getResTitle() != null && nd_data.getResTitle().trim().length() != 0) {
                holder.nd_site.setVisibility(View.VISIBLE);
                holder.nd_site.setText(nd_data.getResTitle());
            }else
                holder.nd_site.setVisibility(View.GONE);*/

            if (nd_data.getResTitle() != null && nd_data.getResTitle().trim().length() != 0) {
                holder.nd_site_holder.setVisibility(View.VISIBLE);
                holder.nd_site_title.setText(nd_data.getResTitle());
                if (nd_data.getResource() != null && nd_data.getResource().trim().length() != 0) {
                    holder.nd_site_avatar.setVisibility(View.VISIBLE);
                    Picasso.with(context).load(nd_data.getResource()).transform(new CircleTransform()).placeholder(R.drawable.trending_gradient_shape).into(holder.nd_site_avatar);
                }else {
                    holder.nd_site_avatar.setVisibility(View.GONE);
                }
            }else {
                holder.nd_site_holder.setVisibility(View.GONE);
            }

            /*if (nd_data.getPost() != null)
                holder.nd_post.setText(nd_data.getIfRawPost().substring(0,nd_data.getIfRawPost().substring(0, Math.min(nd_data.getIfRawPost().length(), 200)).lastIndexOf(' ')+1)+"...");
            else*/
                holder.nd_post.setText("");
            if (nd_data.getThumbUrl1() != null && /*nd_data.getThumbUrl1().size() > 0 &&*/ screenInches >= 4.5) {
                holder.nd_title.setMaxLines(5);
                holder.nd_post.setMaxLines(4);
                holder.nd_photo.setVisibility(View.VISIBLE);
                Picasso.with(context).load(nd_data.getThumbUrl1()).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.trending_gradient_shape).into(holder.nd_photo, new Callback() {
                    @Override
                    public void onSuccess() {}
                    @Override
                    public void onError() {
                        Picasso.with(context).load(nd_data.getThumbUrl1()).placeholder(R.drawable.trending_gradient_shape).into(holder.nd_photo);
                    }
                });
            }else {
                holder.nd_title.setMaxLines(8);
                holder.nd_post.setMaxLines(7);
                holder.nd_photo.setVisibility(View.GONE);
            }

            /*if (nd_data.getTopic() != null && nd_data.getTopic().has("name") && nd_data.getTopic().get("name") != null){
                GradientDrawable gd = new GradientDrawable();
                if (nd_data.getTopic().has("color") && nd_data.getTopic().get("color") != null)
                    gd.setColor(Color.parseColor("#99"+nd_data.getTopic().get("color").getAsString().substring(1)));
                else
                    gd.setColor(Color.parseColor("#99ff0000"));
                gd.setCornerRadius(1);
                holder.nd_topic.setBackgroundDrawable(gd);
                holder.nd_topic.setPadding(8,2,8,2);
                holder.nd_topic.setText(nd_data.getTopic().get("name").getAsString());
                holder.nd_topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("FlipAdapter")
                                .setAction("Post Topic Click")
                                .build());
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("_id", nd_data.getTopic().get("_id").getAsJsonObject().get("$id").getAsString());
                        bundle.putString("name", nd_data.getTopic().get("name").getAsString());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.nd_topic_holder.setVisibility(View.VISIBLE);
            }else {
                holder.nd_topic_holder.setVisibility(View.GONE);
            }*/

            if (topic != null){
                GradientDrawable gd = new GradientDrawable();
                gd.setColor(Color.parseColor("#99ff0000"));
                gd.setCornerRadius(1);
                holder.nd_topic.setBackgroundDrawable(gd);
                holder.nd_topic.setPadding(8,2,8,2);
                holder.nd_topic.setText(topic.getTitle());
                holder.nd_topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("FlipAdapter")
                                .setAction("Post Topic Click")
                                .build());
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("id", topic.getId()+"");
                        bundle.putString("title", topic.getTitle());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.nd_topic_holder.setVisibility(View.VISIBLE);
            }else {
                holder.nd_topic_holder.setVisibility(View.GONE);
            }

            /*holder.nd_add_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    ((Application) context.getApplication()).share_to_board(context,nd_data);
                }
            });*/

            /*try {
                ((Application) context.getApplication()).mViews.remove(nd_data.getId());
            }catch (UnsupportedOperationException w){}*/

            /*holder.nd_comment_holder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    ((Application) context.getApplication()).show_login_dialog(context);
                }
            });*/
            /*holder.nd_share_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, nd_data.getResTitle() + ":\n\n"+nd_data.getTitle() + "\n\nhttp://bekhan.me/s/"+nd_data.getId()+"\n\n"+nd_data.getIfRawPost().toString().substring(0,nd_data.getIfRawPost().substring(0, Math.min(nd_data.getIfRawPost().length(), 400)).lastIndexOf(' ')+1)+"..."+"\n\n\n"+"منتشر شده در مجلو");
                    sendIntent.setType("text/plain");
                    context.startActivity(sendIntent);
                }
            });*/

            holder.rd_holder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("FlipAdapter")
                            .setAction("Post Open")
                            .setLabel("st_rd")
                            .build());
                    Intent intent = new Intent(context, PostViewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("object", new Gson().toJson(rd_data));
                    //bundle.putString("id", st_data.getId());
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                }
            });
            holder.rd_holder.setLongClickable(true);
            holder.rd_title.setText(rd_data.getTitle()/*.substring(0, Math.min(rd_data.getTitle().length(), 100))*/);
            /*if (rd_data.getPost() != null)
                holder.rd_post.setText(rd_data.getIfRawPost().substring(0,rd_data.getIfRawPost().substring(0, Math.min(rd_data.getIfRawPost().length(), 200)).lastIndexOf(' ')+1)+"...");
            else*/
                holder.rd_post.setText("");
            /*if (rd_data.getResTitle() != null && rd_data.getResTitle().trim().length() != 0) {
                holder.rd_site.setVisibility(View.VISIBLE);
                holder.rd_site.setText(rd_data.getResTitle());
            }else
                holder.rd_site.setVisibility(View.GONE);*/

            if (rd_data.getResTitle() != null && rd_data.getResTitle().trim().length() != 0) {
                holder.rd_site_holder.setVisibility(View.VISIBLE);
                holder.rd_site_title.setText(rd_data.getResTitle());
                if (rd_data.getResource() != null && rd_data.getResource().trim().length() != 0) {
                    holder.rd_site_avatar.setVisibility(View.VISIBLE);
                    Picasso.with(context).load(rd_data.getResource()).transform(new CircleTransform()).placeholder(R.drawable.trending_gradient_shape).into(holder.rd_site_avatar);
                }else {
                    holder.rd_site_avatar.setVisibility(View.GONE);
                }
            }else {
                holder.rd_site_holder.setVisibility(View.GONE);
            }

            if (rd_data.getThumbUrl1() != null && /*rd_data.getThumbUrl1().size() > 0 &&*/ screenInches >= 4.5) {
                holder.rd_title.setMaxLines(5);
                holder.rd_post.setMaxLines(4);
                holder.rd_photo.setVisibility(View.VISIBLE);
                Picasso.with(context).load(rd_data.getThumbUrl1()).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.trending_gradient_shape).into(holder.rd_photo, new Callback() {
                    @Override
                    public void onSuccess() {}
                    @Override
                    public void onError() {
                        Picasso.with(context).load(rd_data.getThumbUrl1()).placeholder(R.drawable.trending_gradient_shape).into(holder.rd_photo);
                    }
                });
            }else {
                holder.rd_title.setMaxLines(8);
                holder.rd_post.setMaxLines(7);
                holder.rd_photo.setVisibility(View.GONE);
            }

            /*if (rd_data.getTopic() != null && rd_data.getTopic().has("name") && rd_data.getTopic().get("name") != null){
                GradientDrawable gd = new GradientDrawable();
                if (rd_data.getTopic().has("color") && rd_data.getTopic().get("color") != null)
                    gd.setColor(Color.parseColor("#99"+rd_data.getTopic().get("color").getAsString().substring(1)));
                else
                    gd.setColor(Color.parseColor("#99ff0000"));
                gd.setCornerRadius(1);
                holder.rd_topic.setBackgroundDrawable(gd);
                holder.rd_topic.setPadding(8,2,8,2);
                holder.rd_topic.setText(rd_data.getTopic().get("name").getAsString());
                holder.rd_topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("FlipAdapter")
                                .setAction("Post Topic Click")
                                .build());
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("_id", rd_data.getTopic().get("_id").getAsJsonObject().get("$id").getAsString());
                        bundle.putString("name", rd_data.getTopic().get("name").getAsString());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.rd_topic_holder.setVisibility(View.VISIBLE);
            }else {
                holder.rd_topic_holder.setVisibility(View.GONE);
            }*/

            if (topic != null){
                GradientDrawable gd = new GradientDrawable();
                gd.setColor(Color.parseColor("#99ff0000"));
                gd.setCornerRadius(1);
                holder.rd_topic.setBackgroundDrawable(gd);
                holder.rd_topic.setPadding(8,2,8,2);
                holder.rd_topic.setText(topic.getTitle());
                holder.rd_topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("FlipAdapter")
                                .setAction("Post Topic Click")
                                .build());
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("id", topic.getId()+"");
                        bundle.putString("title", topic.getTitle());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.rd_topic_holder.setVisibility(View.VISIBLE);
            }else {
                holder.rd_topic_holder.setVisibility(View.GONE);
            }

            /*holder.rd_add_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    ((Application) context.getApplication()).share_to_board(context,rd_data);
                }
            });*/

            /*try {
                ((Application) context.getApplication()).mViews.remove(rd_data.getId());
            }catch (UnsupportedOperationException w){}*/

            /*holder.rd_share_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, rd_data.getResTitle() + ":\n\n"+rd_data.getTitle() + "\n\nhttp://bekhan.me/s/"+rd_data.getId()+"\n\n"+rd_data.getIfRawPost().toString().substring(0,rd_data.getIfRawPost().substring(0, Math.min(rd_data.getIfRawPost().length(), 400)).lastIndexOf(' ')+1)+"..."+"\n\n\n"+"منتشر شده در مجلو");
                    sendIntent.setType("text/plain");
                    context.startActivity(sendIntent);
                }
            });*/

        }else if (type == 2){

            final Post st_data;
            final Post nd_data;

            if (data == 1){
                st_data = postList.get(start);
                nd_data = postList.get(start + 1);
            }else{
                st_data = postList.get(start + 1);
                nd_data = postList.get(start);
            }

            if(convertView == null){
                convertView = inflater.inflate(R.layout.feed_template_2, parent, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            }else{
                holder = (ViewHolder) convertView.getTag();
            }

            holder.st_holder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("FlipAdapter")
                            .setAction("Post Open")
                            .setLabel("nd_st")
                            .build());
                    Intent intent = new Intent(context, PostViewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("object", new Gson().toJson(st_data));
                    //bundle.putString("id", st_data.getId());
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                }
            });
            holder.st_holder.setLongClickable(true);
            holder.st_title.setText(st_data.getTitle());

            if (st_data.getResTitle() != null && st_data.getResTitle().trim().length() != 0) {
                holder.st_site_holder.setVisibility(View.VISIBLE);
                //holder.st_site.setText(st_data.getResTitle());
                holder.st_site_title.setText(st_data.getResTitle());
                //holder.st_site_description.setText(st_data.getSite().get("description").getAsString());
                if (st_data.getResource() != null && st_data.getResource().trim().length() != 0) {
                    holder.st_site_avatar.setVisibility(View.VISIBLE);
                    Picasso.with(context).load(st_data.getResource()).transform(new CircleTransform()).placeholder(R.drawable.trending_gradient_shape).into(holder.st_site_avatar);
                }else {
                    holder.st_site_avatar.setVisibility(View.GONE);
                }
            }else {
                holder.st_site_holder.setVisibility(View.GONE);
            }
            /*if (st_data.getPost() != null)
                holder.st_post.setText(st_data.getIfRawPost().substring(0,st_data.getIfRawPost().substring(0, Math.min(st_data.getIfRawPost().length(), 200)).lastIndexOf(' ')+1)+"...");
            else*/
                holder.st_post.setText("");
            if (st_data.getThumbUrl1() != null/* && st_data.getThumbUrl1().size() > 0*/) {
                holder.st_post.setMaxLines(7);
                holder.st_photo_holder.setVisibility(View.VISIBLE);
                Picasso.with(context).load(st_data.getThumbUrl1()).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.trending_gradient_shape).into(holder.st_photo, new Callback() {
                    @Override
                    public void onSuccess() {}
                    @Override
                    public void onError() {
                        Picasso.with(context).load(st_data.getThumbUrl1()).placeholder(R.drawable.trending_gradient_shape).into(holder.st_photo);
                    }
                });
            }else {
                holder.st_post.setMaxLines(5);
                holder.st_photo_holder.setVisibility(View.GONE);
            }

            /*if (st_data.getTopic() != null && st_data.getTopic().has("name") && st_data.getTopic().get("name") != null){
                GradientDrawable gd = new GradientDrawable();
                if (st_data.getTopic().has("color") && st_data.getTopic().get("color") != null)
                    gd.setColor(Color.parseColor("#99"+st_data.getTopic().get("color").getAsString().substring(1)));
                else
                    gd.setColor(Color.parseColor("#99ff0000"));
                gd.setCornerRadius(1);
                holder.st_topic.setBackgroundDrawable(gd);
                holder.st_topic.setPadding(8,2,8,2);
                holder.st_topic.setText(st_data.getTopic().get("name").getAsString());
                holder.st_topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("FlipAdapter")
                                .setAction("Post Topic Click")
                                .build());
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("_id", st_data.getTopic().get("_id").getAsJsonObject().get("$id").getAsString());
                        bundle.putString("name", st_data.getTopic().get("name").getAsString());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.st_topic_holder.setVisibility(View.VISIBLE);
            }else {
                holder.st_topic_holder.setVisibility(View.GONE);
            }*/


            if (topic != null){
                GradientDrawable gd = new GradientDrawable();
                gd.setColor(Color.parseColor("#99ff0000"));
                gd.setCornerRadius(1);
                holder.st_topic.setBackgroundDrawable(gd);
                holder.st_topic.setPadding(8,2,8,2);
                holder.st_topic.setText(topic.getTitle());
                holder.st_topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("FlipAdapter")
                                .setAction("Post Topic Click")
                                .build());
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("id", topic.getId()+"");
                        bundle.putString("title", topic.getTitle());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.st_topic_holder.setVisibility(View.VISIBLE);
            }else {
                holder.st_topic_holder.setVisibility(View.GONE);
            }

            /*holder.st_topic_holder.removeAllViews();
            for (final Map.Entry<String, String> tag:st_data.getTopics().entrySet()){
                TextView t = new TextView(context);
                t.setText(tag.getValue());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(5,0,5,0);
                t.setLayoutParams(params);
                t.setTextColor(Color.parseColor("#ffffff"));
                t.setTextSize(14);
                t.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/IRANSansMobile.ttf"));
                t.setBackgroundResource(R.drawable.topic_button_feed);
                t.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("_id", tag.getKey());
                        bundle.putString("name", tag.getValue());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.st_topic_holder.addView(t);
            }*/
            /*holder.st_add_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    ((Application) context.getApplication()).share_to_board(context,st_data);
                }
            });*/

            /*try {
                ((Application) context.getApplication()).mViews.remove(st_data.getId());
            }catch (UnsupportedOperationException w){}*/

            /*holder.st_share_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, st_data.getResTitle() + ":\n\n"+st_data.getTitle() + "\n\nhttp://bekhan.me/s/"+st_data.getId()+"\n\n"+st_data.getIfRawPost().toString().substring(0,st_data.getIfRawPost().substring(0, Math.min(st_data.getIfRawPost().length(), 400)).lastIndexOf(' ')+1)+"..."+"\n\n\n"+"منتشر شده در مجلو");
                    sendIntent.setType("text/plain");
                    context.startActivity(sendIntent);
                }
            });*/


            holder.nd_holder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("FlipAdapter")
                            .setAction("Post Open")
                            .setLabel("nd_nd")
                            .build());
                    Intent intent = new Intent(context, PostViewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("object", new Gson().toJson(nd_data));
                    //bundle.putString("id", st_data.getId());
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                }
            });
            holder.nd_holder.setLongClickable(true);
            holder.nd_title.setText(nd_data.getTitle()/*.substring(0, Math.min(nd_data.getTitle().length(), 100))*/);
            /*if (nd_data.getResTitle() != null && nd_data.getResTitle().trim().length() != 0) {
                holder.nd_site.setVisibility(View.VISIBLE);
                holder.nd_site.setText(nd_data.getResTitle());
            }else
                holder.nd_site.setVisibility(View.GONE);*/
            if (nd_data.getResTitle() != null && nd_data.getResTitle().trim().length() != 0) {
                holder.nd_site_holder.setVisibility(View.VISIBLE);
                holder.nd_site_title.setText(nd_data.getResTitle());
                if (nd_data.getResource() != null && nd_data.getResource().trim().length() != 0) {
                    holder.nd_site_avatar.setVisibility(View.VISIBLE);
                    Picasso.with(context).load(nd_data.getResource()).transform(new CircleTransform()).placeholder(R.drawable.trending_gradient_shape).into(holder.nd_site_avatar);
                }else {
                    holder.nd_site_avatar.setVisibility(View.GONE);
                }
            }else {
                holder.nd_site_holder.setVisibility(View.GONE);
            }
            /*if (nd_data.getPost() != null)
                holder.nd_post.setText(nd_data.getIfRawPost().substring(0,nd_data.getIfRawPost().substring(0, Math.min(nd_data.getIfRawPost().length(), 200)).lastIndexOf(' ')+1)+"...");
            else*/
                holder.nd_post.setText("");
            if (nd_data.getThumbUrl1() != null /*&& nd_data.getThumbUrl1().size() > 0*/) {
                holder.nd_post.setMaxLines(7);
                holder.nd_photo_holder.setVisibility(View.VISIBLE);
                Picasso.with(context).load(nd_data.getThumbUrl1()).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.trending_gradient_shape).into(holder.nd_photo, new Callback() {
                    @Override
                    public void onSuccess() {}
                    @Override
                    public void onError() {
                        Picasso.with(context).load(nd_data.getThumbUrl1()).placeholder(R.drawable.trending_gradient_shape).into(holder.nd_photo);
                    }
                });
            }else {
                holder.nd_post.setMaxLines(5);
                holder.nd_photo_holder.setVisibility(View.GONE);
            }

            /*if (nd_data.getTopic() != null && nd_data.getTopic().has("name") && nd_data.getTopic().get("name") != null){
                GradientDrawable gd = new GradientDrawable();
                if (nd_data.getTopic().has("color") && nd_data.getTopic().get("color") != null)
                    gd.setColor(Color.parseColor("#99"+nd_data.getTopic().get("color").getAsString().substring(1)));
                else
                    gd.setColor(Color.parseColor("#99ff0000"));
                gd.setCornerRadius(1);
                holder.nd_topic.setBackgroundDrawable(gd);
                holder.nd_topic.setPadding(8,2,8,2);
                holder.nd_topic.setText(nd_data.getTopic().get("name").getAsString());
                holder.nd_topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("FlipAdapter")
                                .setAction("Post Topic Click")
                                .build());
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("_id", nd_data.getTopic().get("_id").getAsJsonObject().get("$id").getAsString());
                        bundle.putString("name", nd_data.getTopic().get("name").getAsString());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.nd_topic_holder.setVisibility(View.VISIBLE);
            }else {
                holder.nd_topic_holder.setVisibility(View.GONE);
            }*/
            if (topic != null){
                GradientDrawable gd = new GradientDrawable();
                gd.setColor(Color.parseColor("#99ff0000"));
                gd.setCornerRadius(1);
                holder.nd_topic.setBackgroundDrawable(gd);
                holder.nd_topic.setPadding(8,2,8,2);
                holder.nd_topic.setText(topic.getTitle());
                holder.nd_topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("FlipAdapter")
                                .setAction("Post Topic Click")
                                .build());
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("id", topic.getId()+"");
                        bundle.putString("title", topic.getTitle());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.nd_topic_holder.setVisibility(View.VISIBLE);
            }else {
                holder.nd_topic_holder.setVisibility(View.GONE);
            }

            /*holder.nd_topic_holder.removeAllViews();
            for (final Map.Entry<String, String> tag:nd_data.getTopics().entrySet()){
                TextView t = new TextView(context);
                t.setText(tag.getValue());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(5,0,5,0);
                t.setLayoutParams(params);
                t.setTextColor(Color.parseColor("#ffffff"));
                t.setTextSize(14);
                t.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/IRANSansMobile.ttf"));
                t.setBackgroundResource(R.drawable.topic_button_feed);
                t.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("_id", tag.getKey());
                        bundle.putString("name", tag.getValue());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.nd_topic_holder.addView(t);
            }*/
            /*holder.nd_add_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    ((Application) context.getApplication()).share_to_board(context,nd_data);
                }
            });*/

            /*try {
                ((Application) context.getApplication()).mViews.remove(nd_data.getId());
            }catch (UnsupportedOperationException w){}*/

            /*holder.nd_share_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, nd_data.getResTitle() + ":\n\n"+nd_data.getTitle() + "\n\nhttp://bekhan.me/s/"+nd_data.getId()+"\n\n"+nd_data.getIfRawPost().toString().substring(0,nd_data.getIfRawPost().substring(0, Math.min(nd_data.getIfRawPost().length(), 400)).lastIndexOf(' ')+1)+"..."+"\n\n\n"+"منتشر شده در مجلو");
                    sendIntent.setType("text/plain");
                    context.startActivity(sendIntent);
                }
            });*/
        }else if (type == 3){

            final Post st_data;
            final Post nd_data;
            final Post rd_data;

            if (postList.get(start).getThumbUrl1() != null) {
                rd_data = postList.get(start);
                if (data == 1) {
                    st_data = postList.get(start + 1);
                    nd_data = postList.get(start + 2);
                } else {
                    st_data = postList.get(start + 2);
                    nd_data = postList.get(start + 1);
                }
            }else if (postList.get(start+1).getThumbUrl1() != null) {
                rd_data = postList.get(start + 1);
                if (data == 1) {
                    st_data = postList.get(start);
                    nd_data = postList.get(start + 2);
                } else {
                    st_data = postList.get(start + 2);
                    nd_data = postList.get(start);
                }
            }else {
                rd_data = postList.get(start + 2);
                if (data == 1) {
                    st_data = postList.get(start + 1);
                    nd_data = postList.get(start);
                } else {
                    st_data = postList.get(start);
                    nd_data = postList.get(start + 1);
                }
            }

            if(convertView == null){
                convertView = inflater.inflate(R.layout.feed_template_3, parent, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            }else{
                holder = (ViewHolder) convertView.getTag();
            }

            holder.st_holder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("FlipAdapter")
                            .setAction("Post Open")
                            .setLabel("rd_st")
                            .build());
                    Intent intent = new Intent(context, PostViewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("object", new Gson().toJson(st_data));
                    //bundle.putString("id", st_data.getId());
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                }
            });
            holder.st_holder.setLongClickable(true);
            holder.st_title.setText(st_data.getTitle());
            if (st_data.getResTitle() != null && st_data.getResTitle().trim().length() != 0) {
                holder.st_site_holder.setVisibility(View.VISIBLE);
                //holder.st_site.setText(st_data.getResTitle());
                holder.st_site_title.setText(st_data.getResTitle());
                //holder.st_site_description.setText(st_data.getSite().get("description").getAsString());
                if (st_data.getResource() != null && st_data.getResource().trim().length() != 0) {
                    holder.st_site_avatar.setVisibility(View.VISIBLE);
                    Picasso.with(context).load(st_data.getResource()).transform(new CircleTransform()).placeholder(R.drawable.trending_gradient_shape).into(holder.st_site_avatar);
                }else {
                    holder.st_site_avatar.setVisibility(View.GONE);
                }
            }else {
                holder.st_site_holder.setVisibility(View.GONE);
            }
            /*if (st_data.getResTitle() != null && st_data.getResTitle().trim().length() != 0) {
                holder.st_site.setVisibility(View.VISIBLE);
                holder.st_site.setText(st_data.getResTitle());
            }else
                holder.st_site.setVisibility(View.GONE);*/
            /*if (st_data.getPost() != null)
                holder.st_post.setText(st_data.getIfRawPost().substring(0,st_data.getIfRawPost().substring(0, Math.min(st_data.getIfRawPost().length(), 500)).lastIndexOf(' ')+1)+"...");
            else*/
                holder.st_post.setText("");
            if (st_data.getThumbUrl1() != null /*&& st_data.getThumbUrl1().size() > 0*/) {
                holder.st_post.setMaxLines(3);
                holder.st_photo_holder.setVisibility(View.VISIBLE);
                Picasso.with(context).load(st_data.getThumbUrl1()).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.trending_gradient_shape).into(holder.st_photo, new Callback() {
                    @Override
                    public void onSuccess() {}
                    @Override
                    public void onError() {
                        Picasso.with(context).load(st_data.getThumbUrl1()).placeholder(R.drawable.trending_gradient_shape).into(holder.st_photo);
                    }
                });
            }else {
                holder.st_post.setMaxLines(10);
                holder.st_photo_holder.setVisibility(View.GONE);
            }

            /*if (st_data.getTopic() != null && st_data.getTopic().has("name") && st_data.getTopic().get("name") != null){
                GradientDrawable gd = new GradientDrawable();
                if (st_data.getTopic().has("color") && st_data.getTopic().get("color") != null)
                    gd.setColor(Color.parseColor("#99"+st_data.getTopic().get("color").getAsString().substring(1)));
                else
                    gd.setColor(Color.parseColor("#99ff0000"));
                gd.setCornerRadius(1);
                holder.st_topic.setBackgroundDrawable(gd);
                holder.st_topic.setPadding(8,2,8,2);
                holder.st_topic.setText(st_data.getTopic().get("name").getAsString());
                holder.st_topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("FlipAdapter")
                                .setAction("Post Topic Click")
                                .build());
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("_id", st_data.getTopic().get("_id").getAsJsonObject().get("$id").getAsString());
                        bundle.putString("name", st_data.getTopic().get("name").getAsString());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.st_topic_holder.setVisibility(View.VISIBLE);
            }else {
                holder.st_topic_holder.setVisibility(View.GONE);
            }*/


            if (topic != null){
                GradientDrawable gd = new GradientDrawable();
                gd.setColor(Color.parseColor("#99ff0000"));
                gd.setCornerRadius(1);
                holder.st_topic.setBackgroundDrawable(gd);
                holder.st_topic.setPadding(8,2,8,2);
                holder.st_topic.setText(topic.getTitle());
                holder.st_topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("FlipAdapter")
                                .setAction("Post Topic Click")
                                .build());
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("id", topic.getId()+"");
                        bundle.putString("title", topic.getTitle());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.st_topic_holder.setVisibility(View.VISIBLE);
            }else {
                holder.st_topic_holder.setVisibility(View.GONE);
            }

            /*holder.st_add_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    ((Application) context.getApplication()).share_to_board(context,st_data);
                }
            });*/

            /*try {
                ((Application) context.getApplication()).mViews.remove(st_data.getId());
            }catch (UnsupportedOperationException w){}*/

            /*holder.st_share_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, st_data.getResTitle() + ":\n\n"+st_data.getTitle() + "\n\nhttp://bekhan.me/s/"+st_data.getId()+"\n\n"+st_data.getIfRawPost().toString().substring(0,st_data.getIfRawPost().substring(0, Math.min(st_data.getIfRawPost().length(), 400)).lastIndexOf(' ')+1)+"..."+"\n\n\n"+"منتشر شده در مجلو");
                    sendIntent.setType("text/plain");
                    context.startActivity(sendIntent);
                }
            });*/

            holder.nd_holder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("FlipAdapter")
                            .setAction("Post Open")
                            .setLabel("rd_nd")
                            .build());
                    Intent intent = new Intent(context, PostViewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("object", new Gson().toJson(nd_data));
                    //bundle.putString("id", st_data.getId());
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                }
            });
            holder.nd_holder.setLongClickable(true);
            holder.nd_title.setText(nd_data.getTitle()/*.substring(0, Math.min(nd_data.getTitle().length(), 100))*/);
            /*if (nd_data.getResTitle() != null && nd_data.getResTitle().trim().length() != 0) {
                holder.nd_site.setVisibility(View.VISIBLE);
                holder.nd_site.setText(nd_data.getResTitle());
            }else
                holder.nd_site.setVisibility(View.GONE);*/
            if (nd_data.getResTitle() != null && nd_data.getResTitle().trim().length() != 0) {
                holder.nd_site_holder.setVisibility(View.VISIBLE);
                holder.nd_site_title.setText(nd_data.getResTitle());
                if (nd_data.getResource() != null && nd_data.getResource().trim().length() != 0) {
                    holder.nd_site_avatar.setVisibility(View.VISIBLE);
                    Picasso.with(context).load(nd_data.getResource()).transform(new CircleTransform()).placeholder(R.drawable.trending_gradient_shape).into(holder.nd_site_avatar);
                }else {
                    holder.nd_site_avatar.setVisibility(View.GONE);
                }
            }else {
                holder.nd_site_holder.setVisibility(View.GONE);
            }
            /*if (nd_data.getPost() != null)
                holder.nd_post.setText(nd_data.getIfRawPost().substring(0,nd_data.getIfRawPost().substring(0, Math.min(nd_data.getIfRawPost().length(), 500)).lastIndexOf(' ')+1)+"...");
            else*/
                holder.nd_post.setText("");
            if (nd_data.getThumbUrl1() != null /*&& nd_data.getThumbUrl1().size() > 0*/) {
                holder.nd_post.setMaxLines(3);
                holder.nd_photo_holder.setVisibility(View.VISIBLE);
                holder.nd_photo.setVisibility(View.VISIBLE);
                Picasso.with(context).load(nd_data.getThumbUrl1()).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.trending_gradient_shape).into(holder.nd_photo, new Callback() {
                    @Override
                    public void onSuccess() {}
                    @Override
                    public void onError() {
                        Picasso.with(context).load(nd_data.getThumbUrl1()).placeholder(R.drawable.trending_gradient_shape).into(holder.nd_photo);
                    }
                });
            }else {
                holder.nd_post.setMaxLines(10);
                holder.nd_photo_holder.setVisibility(View.GONE);
                holder.nd_photo.setVisibility(View.GONE);
            }

            /*if (nd_data.getTopic() != null && nd_data.getTopic().has("name") && nd_data.getTopic().get("name") != null){
                GradientDrawable gd = new GradientDrawable();
                if (nd_data.getTopic().has("color") && nd_data.getTopic().get("color") != null)
                    gd.setColor(Color.parseColor("#99"+nd_data.getTopic().get("color").getAsString().substring(1)));
                else
                    gd.setColor(Color.parseColor("#99ff0000"));
                gd.setCornerRadius(1);
                holder.nd_topic.setBackgroundDrawable(gd);
                holder.nd_topic.setPadding(8,2,8,2);
                holder.nd_topic.setText(nd_data.getTopic().get("name").getAsString());
                holder.nd_topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("FlipAdapter")
                                .setAction("Post Topic Click")
                                .build());
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("_id", nd_data.getTopic().get("_id").getAsJsonObject().get("$id").getAsString());
                        bundle.putString("name", nd_data.getTopic().get("name").getAsString());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.nd_topic_holder.setVisibility(View.VISIBLE);
            }else {
                holder.nd_topic_holder.setVisibility(View.GONE);
            }*/

            if (topic != null){
                GradientDrawable gd = new GradientDrawable();
                gd.setColor(Color.parseColor("#99ff0000"));
                gd.setCornerRadius(1);
                holder.nd_topic.setBackgroundDrawable(gd);
                holder.nd_topic.setPadding(8,2,8,2);
                holder.nd_topic.setText(topic.getTitle());
                holder.nd_topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("FlipAdapter")
                                .setAction("Post Topic Click")
                                .build());
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("id", topic.getId()+"");
                        bundle.putString("title", topic.getTitle());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.nd_topic_holder.setVisibility(View.VISIBLE);
            }else {
                holder.nd_topic_holder.setVisibility(View.GONE);
            }

            /*holder.nd_add_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    ((Application) context.getApplication()).share_to_board(context,nd_data);
                }
            });*/

            /*try {
                ((Application) context.getApplication()).mViews.remove(nd_data.getId());
            }catch (UnsupportedOperationException w){}*/

            /*holder.nd_share_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, nd_data.getResTitle() + ":\n\n"+nd_data.getTitle() + "\n\nhttp://bekhan.me/s/"+nd_data.getId()+"\n\n"+nd_data.getIfRawPost().toString().substring(0,nd_data.getIfRawPost().substring(0, Math.min(nd_data.getIfRawPost().length(), 400)).lastIndexOf(' ')+1)+"..."+"\n\n\n"+"منتشر شده در مجلو");
                    sendIntent.setType("text/plain");
                    context.startActivity(sendIntent);
                }
            });*/


            holder.rd_holder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("FlipAdapter")
                            .setAction("Post Open")
                            .setLabel("rd_rd")
                            .build());
                    Intent intent = new Intent(context, PostViewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("object", new Gson().toJson(rd_data));
                    //bundle.putString("id", st_data.getId());
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                }
            });
            holder.rd_holder.setLongClickable(true);
            holder.rd_title.setText(rd_data.getTitle()/*.substring(0, Math.min(rd_data.getTitle().length(), 100))*/);
            /*if (rd_data.getResTitle() != null && rd_data.getResTitle().trim().length() != 0) {
                holder.rd_site.setVisibility(View.VISIBLE);
                holder.rd_site.setText(rd_data.getResTitle());
            }else
                holder.rd_site.setVisibility(View.GONE);*/
            if (rd_data.getResTitle() != null && rd_data.getResTitle().trim().length() != 0) {
                holder.rd_site_holder.setVisibility(View.VISIBLE);
                holder.rd_site_title.setText(rd_data.getResTitle());
                if (rd_data.getResource() != null && rd_data.getResource().trim().length() != 0) {
                    holder.rd_site_avatar.setVisibility(View.VISIBLE);
                    Picasso.with(context).load(rd_data.getResource()).transform(new CircleTransform()).placeholder(R.drawable.trending_gradient_shape).into(holder.rd_site_avatar);
                }else {
                    holder.rd_site_avatar.setVisibility(View.GONE);
                }
            }else {
                holder.rd_site_holder.setVisibility(View.GONE);
            }
            /*if (rd_data.getPost() != null)
                holder.rd_post.setText(rd_data.getIfRawPost().substring(0,rd_data.getIfRawPost().substring(0, Math.min(rd_data.getIfRawPost().length(), 200)).lastIndexOf(' ')+1)+"...");
            else*/
                holder.rd_post.setText("");
            if (rd_data.getThumbUrl1() != null /*&& rd_data.getThumbUrl1().size() > 0*/) {
                holder.rd_post.setMaxLines(7);
                holder.rd_photo.setVisibility(View.VISIBLE);
                holder.rd_photo_holder.setVisibility(View.VISIBLE);
                Picasso.with(context).load(rd_data.getThumbUrl1()).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.trending_gradient_shape).into(holder.rd_photo, new Callback() {
                    @Override
                    public void onSuccess() {}
                    @Override
                    public void onError() {
                        Picasso.with(context).load(rd_data.getThumbUrl1()).placeholder(R.drawable.trending_gradient_shape).into(holder.rd_photo);
                    }
                });
            }else {
                holder.rd_post.setMaxLines(5);
                holder.rd_photo.setVisibility(View.GONE);
                holder.rd_photo_holder.setVisibility(View.GONE);
            }

            /*if (rd_data.getTopic() != null && rd_data.getTopic().has("name") && rd_data.getTopic().get("name") != null){
                GradientDrawable gd = new GradientDrawable();
                if (rd_data.getTopic().has("color") && rd_data.getTopic().get("color") != null)
                    gd.setColor(Color.parseColor("#99"+rd_data.getTopic().get("color").getAsString().substring(1)));
                else
                    gd.setColor(Color.parseColor("#99ff0000"));
                gd.setCornerRadius(1);
                holder.rd_topic.setBackgroundDrawable(gd);
                holder.rd_topic.setPadding(8,2,8,2);
                holder.rd_topic.setText(rd_data.getTopic().get("name").getAsString());
                holder.rd_topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("FlipAdapter")
                                .setAction("Post Topic Click")
                                .build());
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("_id", rd_data.getTopic().get("_id").getAsJsonObject().get("$id").getAsString());
                        bundle.putString("name", rd_data.getTopic().get("name").getAsString());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.rd_topic_holder.setVisibility(View.VISIBLE);
            }else {
                holder.rd_topic_holder.setVisibility(View.GONE);
            }*/

            if (topic != null){
                GradientDrawable gd = new GradientDrawable();
                gd.setColor(Color.parseColor("#99ff0000"));
                gd.setCornerRadius(1);
                holder.rd_topic.setBackgroundDrawable(gd);
                holder.rd_topic.setPadding(8,2,8,2);
                holder.rd_topic.setText(topic.getTitle());
                holder.rd_topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("FlipAdapter")
                                .setAction("Post Topic Click")
                                .build());
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("id", topic.getId()+"");
                        bundle.putString("title", topic.getTitle());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.rd_topic_holder.setVisibility(View.VISIBLE);
            }else {
                holder.rd_topic_holder.setVisibility(View.GONE);
            }

            /*holder.rd_topic_holder.removeAllViews();
            for (final Map.Entry<String, String> tag:rd_data.getTopics().entrySet()){
                TextView t = new TextView(context);
                t.setText(tag.getValue());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(5,0,5,0);
                t.setLayoutParams(params);
                t.setTextColor(Color.parseColor("#ffffff"));
                t.setTextSize(14);
                t.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/IRANSansMobile.ttf"));
                t.setBackgroundResource(R.drawable.topic_button_feed);
                t.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("_id", tag.getKey());
                        bundle.putString("name", tag.getValue());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.rd_topic_holder.addView(t);
            }*/
            /*holder.rd_add_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    ((Application) context.getApplication()).share_to_board(context,rd_data);
                }
            });*/

            /*try {
                ((Application) context.getApplication()).mViews.remove(rd_data.getId());
            }catch (UnsupportedOperationException w){}*/

            /*holder.rd_share_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, rd_data.getResTitle() + ":\n\n"+rd_data.getTitle() + "\n\nhttp://bekhan.me/s/"+rd_data.getId()+"\n\n"+rd_data.getIfRawPost().toString().substring(0,rd_data.getIfRawPost().substring(0, Math.min(rd_data.getIfRawPost().length(), 400)).lastIndexOf(' ')+1)+"..."+"\n\n\n"+"منتشر شده در مجلو");
                    sendIntent.setType("text/plain");
                    context.startActivity(sendIntent);
                }
            });*/

        }else if (type == 4){

            final Post st_data = postList.get(start);

            if(convertView == null){
                convertView = inflater.inflate(R.layout.feed_template_4, parent, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            }else{
                holder = (ViewHolder) convertView.getTag();
            }

            holder.st_holder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("FlipAdapter")
                            .setAction("Post Open")
                            .setLabel("4th_st")
                            .build());
                    Intent intent = new Intent(context, PostViewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("object", new Gson().toJson(st_data));
                    //bundle.putString("id", st_data.getId());
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                }
            });
            holder.st_holder.setLongClickable(true);
            holder.st_title.setText(st_data.getTitle());
            /*if (st_data.getResTitle() != null && st_data.getResTitle().trim().length() != 0) {
                holder.st_site.setVisibility(View.VISIBLE);
                holder.st_site.setText(st_data.getResTitle());
            }else
                holder.st_site.setVisibility(View.GONE);*/

            if (st_data.getResTitle() != null && st_data.getResTitle().trim().length() != 0) {
                holder.st_site_holder.setVisibility(View.VISIBLE);
                //holder.st_site.setText(st_data.getResTitle());
                holder.st_site_title.setText(st_data.getResTitle());
                //holder.st_site_description.setText(st_data.getSite().get("description").getAsString());
                if (st_data.getResource() != null && st_data.getResource().trim().length() != 0) {
                    holder.st_site_avatar.setVisibility(View.VISIBLE);
                    Picasso.with(context).load(st_data.getResource()).transform(new CircleTransform()).placeholder(R.drawable.trending_gradient_shape).into(holder.st_site_avatar);
                }else {
                    holder.st_site_avatar.setVisibility(View.GONE);
                }
            }else {
                holder.st_site_holder.setVisibility(View.GONE);
            }

            /*if (st_data.getPost() != null)
                holder.st_post.setText(st_data.getIfRawPost().substring(0,st_data.getIfRawPost().substring(0, Math.min(st_data.getIfRawPost().length(), 200)).lastIndexOf(' ')+1)+"...");
            else*/
                holder.st_post.setText("");
            if (st_data.getThumbUrl1() != null /*&& st_data.getThumbUrl1().size() > 0*/) {
                holder.st_photo.setVisibility(View.VISIBLE);
                Picasso.with(context).load(st_data.getThumbUrl1()).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.trending_gradient_shape).into(holder.st_photo, new Callback() {
                    @Override
                    public void onSuccess() {}
                    @Override
                    public void onError() {
                        Picasso.with(context).load(st_data.getThumbUrl1()).placeholder(R.drawable.trending_gradient_shape).into(holder.st_photo);
                    }
                });
            }else {
                holder.st_photo.setVisibility(View.GONE);
            }

            /*if (st_data.getTopic() != null && st_data.getTopic().has("name") && st_data.getTopic().get("name") != null){
                GradientDrawable gd = new GradientDrawable();
                if (st_data.getTopic().has("color") && st_data.getTopic().get("color") != null)
                    gd.setColor(Color.parseColor("#99"+st_data.getTopic().get("color").getAsString().substring(1)));
                else
                    gd.setColor(Color.parseColor("#99ff0000"));
                gd.setCornerRadius(1);
                holder.st_topic.setBackgroundDrawable(gd);
                holder.st_topic.setPadding(8,2,8,2);
                holder.st_topic.setText(st_data.getTopic().get("name").getAsString());
                holder.st_topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("FlipAdapter")
                                .setAction("Post Topic Click")
                                .build());
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("_id", st_data.getTopic().get("_id").getAsJsonObject().get("$id").getAsString());
                        bundle.putString("name", st_data.getTopic().get("name").getAsString());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.st_topic_holder.setVisibility(View.VISIBLE);
            }else {
                holder.st_topic_holder.setVisibility(View.GONE);
            }*/


            if (topic != null){
                GradientDrawable gd = new GradientDrawable();
                gd.setColor(Color.parseColor("#99ff0000"));
                gd.setCornerRadius(1);
                holder.st_topic.setBackgroundDrawable(gd);
                holder.st_topic.setPadding(8,2,8,2);
                holder.st_topic.setText(topic.getTitle());
                holder.st_topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("FlipAdapter")
                                .setAction("Post Topic Click")
                                .build());
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("id", topic.getId()+"");
                        bundle.putString("title", topic.getTitle());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.st_topic_holder.setVisibility(View.VISIBLE);
            }else {
                holder.st_topic_holder.setVisibility(View.GONE);
            }

            /*holder.st_topic_holder.removeAllViews();
            for (final Map.Entry<String, String> tag:st_data.getTopics().entrySet()){
                TextView t = new TextView(context);
                t.setText(tag.getValue());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(5,0,5,0);
                t.setLayoutParams(params);
                t.setTextColor(Color.parseColor("#ffffff"));
                t.setTextSize(14);
                t.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/IRANSansMobile.ttf"));
                t.setBackgroundResource(R.drawable.topic_button_feed);
                t.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("_id", tag.getKey());
                        bundle.putString("name", tag.getValue());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.st_topic_holder.addView(t);
            }*/
            /*holder.st_add_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    ((Application) context.getApplication()).share_to_board(context,st_data);
                }
            });*/

            /*try {
                ((Application) context.getApplication()).mViews.remove(st_data.getId());
            }catch (UnsupportedOperationException w){}*/

            /*holder.st_share_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, st_data.getResTitle() + ":\n\n"+st_data.getTitle() + "\n\nhttp://bekhan.me/s/"+st_data.getId()+"\n\n"+st_data.getIfRawPost().toString().substring(0,st_data.getIfRawPost().substring(0, Math.min(st_data.getIfRawPost().length(), 400)).lastIndexOf(' ')+1)+"..."+"\n\n\n"+"منتشر شده در مجلو");
                    sendIntent.setType("text/plain");
                    context.startActivity(sendIntent);
                }
            });*/

        }else if (type == 7){

            final Post st_data = postList.get(start);

            if(convertView == null){
                convertView = inflater.inflate(R.layout.feed_template_5, parent, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            }else{
                holder = (ViewHolder) convertView.getTag();
            }

            holder.st_holder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("FlipAdapter")
                            .setAction("Post Open")
                            .setLabel("5th_st")
                            .build());
                    Intent intent = new Intent(context, PostViewActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("object", new Gson().toJson(st_data));
                    //bundle.putString("id", st_data.getId());
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                }
            });
            holder.st_holder.setLongClickable(true);
            holder.st_title.setText(st_data.getTitle());

            if (st_data.getResTitle() != null && st_data.getResTitle().trim().length() != 0) {
                holder.st_site_holder.setVisibility(View.VISIBLE);
                //holder.st_site.setText(st_data.getResTitle());
                holder.st_site_title.setText(st_data.getResTitle());
                //holder.st_site_description.setText(st_data.getSite().get("description").getAsString());
                if (st_data.getResource() != null && st_data.getResource().trim().length() != 0) {
                    holder.st_site_avatar.setVisibility(View.VISIBLE);
                    Picasso.with(context).load(st_data.getResource()).transform(new CircleTransform()).placeholder(R.drawable.trending_gradient_shape).into(holder.st_site_avatar);
                } else {
                    holder.st_site_avatar.setVisibility(View.GONE);
                }
            }else {
                holder.st_site_holder.setVisibility(View.GONE);
            }
            /*if (st_data.getPost() != null)
                holder.st_post.setText(st_data.getIfRawPost().substring(0,st_data.getIfRawPost().lastIndexOf(' ')+1)+"...");
            else*/
                holder.st_post.setText("");
            if (st_data.getThumbUrl1() != null /*&& st_data.getThumbUrl1().size() > 0*/) {
                holder.st_photo_holder.setVisibility(View.VISIBLE);
                Picasso.with(context).load(st_data.getThumbUrl1()).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.trending_gradient_shape).into(holder.st_photo, new Callback() {
                    @Override
                    public void onSuccess() {}
                    @Override
                    public void onError() {
                        Picasso.with(context).load(st_data.getThumbUrl1()).placeholder(R.drawable.trending_gradient_shape).into(holder.st_photo);
                    }
                });
            }else {
                holder.st_photo_holder.setVisibility(View.GONE);
            }

            /*if (st_data.getTopic() != null && st_data.getTopic().has("name") && st_data.getTopic().get("name") != null){
                GradientDrawable gd = new GradientDrawable();
                if (st_data.getTopic().has("color") && st_data.getTopic().get("color") != null)
                    gd.setColor(Color.parseColor("#99"+st_data.getTopic().get("color").getAsString().substring(1)));
                else
                    gd.setColor(Color.parseColor("#99ff0000"));
                gd.setCornerRadius(1);
                holder.st_topic.setBackgroundDrawable(gd);
                holder.st_topic.setPadding(8,2,8,2);
                holder.st_topic.setText(st_data.getTopic().get("name").getAsString());
                holder.st_topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("FlipAdapter")
                                .setAction("Post Topic Click")
                                .build());
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("_id", st_data.getTopic().get("_id").getAsJsonObject().get("$id").getAsString());
                        bundle.putString("name", st_data.getTopic().get("name").getAsString());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.st_topic_holder.setVisibility(View.VISIBLE);
            }else {
                holder.st_topic_holder.setVisibility(View.GONE);
            }*/


            if (topic != null){
                GradientDrawable gd = new GradientDrawable();
                gd.setColor(Color.parseColor("#99ff0000"));
                gd.setCornerRadius(1);
                holder.st_topic.setBackgroundDrawable(gd);
                holder.st_topic.setPadding(8,2,8,2);
                holder.st_topic.setText(topic.getTitle());
                holder.st_topic.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTracker.send(new HitBuilders.EventBuilder()
                                .setCategory("FlipAdapter")
                                .setAction("Post Topic Click")
                                .build());
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("id", topic.getId()+"");
                        bundle.putString("title", topic.getTitle());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.st_topic_holder.setVisibility(View.VISIBLE);
            }else {
                holder.st_topic_holder.setVisibility(View.GONE);
            }

            /*holder.st_topic_holder.removeAllViews();
            for (final Map.Entry<String, String> tag:st_data.getTopics().entrySet()){
                TextView t = new TextView(context);
                t.setText(tag.getValue());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.setMargins(5,0,5,0);
                t.setLayoutParams(params);
                t.setTextColor(Color.parseColor("#ffffff"));
                t.setTextSize(14);
                t.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/IRANSansMobile.ttf"));
                t.setBackgroundResource(R.drawable.topic_button_feed);
                t.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, TopicActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("_id", tag.getKey());
                        bundle.putString("name", tag.getValue());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                        //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                    }
                });
                holder.st_topic_holder.addView(t);
            }*/
            /*holder.st_add_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    ((Application) context.getApplication()).share_to_board(context,st_data);
                }
            });*/

            /*try {
                ((Application) context.getApplication()).mViews.remove(st_data.getId());
            }catch (UnsupportedOperationException w){}*/

            /*holder.st_share_text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, st_data.getResTitle() + ":\n\n"+st_data.getTitle() + "\n\nhttp://bekhan.me/s/"+st_data.getId()+"\n\n"+st_data.getIfRawPost().toString().substring(0,st_data.getIfRawPost().substring(0, Math.min(st_data.getIfRawPost().length(), 400)).lastIndexOf(' ')+1)+"..."+"\n\n\n"+"منتشر شده در مجلو");
                    sendIntent.setType("text/plain");
                    context.startActivity(sendIntent);
                }
            });*/

        }else if (type == 6){
            final Post st_data = postList.get(start);

            if(convertView == null){
                convertView = inflater.inflate(R.layout.feed_ads_template, parent, false);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            }else{
                holder = (ViewHolder) convertView.getTag();
            }
            if (st_data.getThumbUrl1() != null) {
                holder.st_photo.setVisibility(View.VISIBLE);
                Picasso.with(context).load(st_data.getThumbUrl1()).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.trending_gradient_shape).into(holder.st_photo, new Callback() {
                    @Override
                    public void onSuccess() {}
                    @Override
                    public void onError() {
                        Picasso.with(context).load(st_data.getThumbUrl1()).placeholder(R.drawable.trending_gradient_shape).into(holder.st_photo);
                    }
                });
            }else
                holder.st_photo.setVisibility(View.GONE);

            holder.st_title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("FlipAdapter")
                            .setAction("Ads Order")
                            .setLabel("ads")
                            .setValue(st_data.getID())
                            .build());

                    new AlertDialog.Builder(context)
                            //.setTitle("تبلیغات در مجلو")
                            .setCustomTitle(((Application) context.getApplication()).DialogTitle(context,"تبلیغات در مجلو"))
                            .setMessage("جهت کسب اطلاعات، با ما در ارتباط باشید: info@bekhan.me")
                            //.setIcon(android.R.drawable.ic_dialog_info)
                            .show();
                }
            });

            holder.st_photo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("FlipAdapter")
                            .setAction("Ads Clicked")
                            .setLabel("ads")
                            .setValue(st_data.getID())
                            .build());
                    Uri uri = Uri.parse("http://bekhan.me/a/"+st_data.getID());
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);
                    context.startActivity(browserIntent);
                }
            });
        }else {
            holder = null;
        }

        /*if (holder != null && holder.st_user_avatar != null)
            Picasso.with(context).load(R.drawable.default_avatar).transform(new CircleTransform()).into(holder.st_user_avatar);
        if (holder != null && holder.nd_user_avatar != null)
            Picasso.with(context).load(R.drawable.default_avatar).transform(new CircleTransform()).into(holder.nd_user_avatar);
        if (holder != null && holder.rd_user_avatar != null)
            Picasso.with(context).load(R.drawable.default_avatar).transform(new CircleTransform()).into(holder.rd_user_avatar);*/

        return convertView;
    }

    static class ViewHolder{
        TextView st_title;
        TextView nd_title;
        TextView rd_title;
        TextView st_post;
        TextView nd_post;
        TextView rd_post;
        ImageView st_photo;
        LinearLayout st_photo_holder;
        LinearLayout st_holder;
        LinearLayout nd_holder;
        LinearLayout rd_holder;
        ImageView nd_photo;
        LinearLayout nd_photo_holder;
        LinearLayout rd_photo_holder;
        ImageView rd_photo;
        LinearLayout st_site_holder;
        LinearLayout nd_site_holder;
        LinearLayout rd_site_holder;
        TextView st_site;
        ImageView st_site_avatar;
        TextView st_site_title;
        TextView st_site_description;
        TextView nd_site;
        ImageView nd_site_avatar;
        TextView nd_site_title;
        TextView rd_site;
        ImageView rd_site_avatar;
        TextView rd_site_title;
        TextView st_topic;
        TextView nd_topic;
        TextView rd_topic;
        LinearLayout st_topic_holder;
        LinearLayout nd_topic_holder;
        LinearLayout rd_topic_holder;
        //ImageButton st_add_button;
        //ImageButton nd_add_button;
        //ImageButton rd_add_button;
        //ImageButton st_share_text;
        //ImageButton nd_share_text;
        //ImageButton rd_share_text;
        //LinearLayout st_comment_holder;
        //LinearLayout nd_comment_holder;
        //LinearLayout rd_comment_holder;
        //ImageView st_user_avatar;
        //ImageView nd_user_avatar;
        //ImageView rd_user_avatar;

        public ViewHolder(View convertView){
            this.st_title = (TextView) convertView.findViewById(R.id.st_title);
            this.st_post = (TextView) convertView.findViewById(R.id.st_post);
            this.st_site_holder = (LinearLayout) convertView.findViewById(R.id.st_site_holder);
            this.st_site = (TextView) convertView.findViewById(R.id.st_site);
            this.st_site_holder = (LinearLayout) convertView.findViewById(R.id.st_site_holder);
            this.st_site_title = (TextView) convertView.findViewById(R.id.st_site_title);
            this.st_site_description = (TextView) convertView.findViewById(R.id.st_site_description);
            this.st_site_avatar = (ImageView) convertView.findViewById(R.id.st_site_avatar);
            this.st_photo = (ImageView) convertView.findViewById(R.id.st_photo);
            this.st_photo_holder = (LinearLayout) convertView.findViewById(R.id.st_photo_holder);
            this.st_holder = (LinearLayout) convertView.findViewById(R.id.st_holder);
            this.st_topic = (TextView) convertView.findViewById(R.id.st_topic);
            this.st_topic_holder = (LinearLayout) convertView.findViewById(R.id.st_topic_holder);
            //this.st_add_button = (ImageButton) convertView.findViewById(R.id.st_add_button);
            //this.st_share_text = (ImageButton) convertView.findViewById(R.id.st_share_text);
            //this.st_comment_holder = (LinearLayout) convertView.findViewById(R.id.st_comment_holder);
            //this.st_user_avatar = (ImageView) convertView.findViewById(R.id.st_user_avatar);

            this.nd_title = (TextView) convertView.findViewById(R.id.nd_title);
            this.nd_post = (TextView) convertView.findViewById(R.id.nd_post);
            this.nd_site = (TextView) convertView.findViewById(R.id.nd_site);
            this.nd_site_holder = (LinearLayout) convertView.findViewById(R.id.nd_site_holder);
            this.nd_site_title = (TextView) convertView.findViewById(R.id.nd_site_title);
            this.nd_site_avatar = (ImageView) convertView.findViewById(R.id.nd_site_avatar);
            this.nd_photo = (ImageView) convertView.findViewById(R.id.nd_photo);
            this.nd_holder = (LinearLayout) convertView.findViewById(R.id.nd_holder);
            this.nd_photo_holder = (LinearLayout) convertView.findViewById(R.id.nd_photo_holder);
            this.nd_topic = (TextView) convertView.findViewById(R.id.nd_topic);
            this.nd_topic_holder = (LinearLayout) convertView.findViewById(R.id.nd_topic_holder);
            //this.nd_add_button = (ImageButton) convertView.findViewById(R.id.nd_add_button);
            //this.nd_share_text = (ImageButton) convertView.findViewById(R.id.nd_share_text);
            //this.nd_comment_holder = (LinearLayout) convertView.findViewById(R.id.nd_comment_holder);
            //this.nd_user_avatar = (ImageView) convertView.findViewById(R.id.nd_user_avatar);

            this.rd_title = (TextView) convertView.findViewById(R.id.rd_title);
            this.rd_post = (TextView) convertView.findViewById(R.id.rd_post);
            //this.rd_site = (TextView) convertView.findViewById(R.id.rd_site);
            this.rd_site_holder = (LinearLayout) convertView.findViewById(R.id.rd_site_holder);
            this.rd_site_title = (TextView) convertView.findViewById(R.id.rd_site_title);
            this.rd_site_avatar = (ImageView) convertView.findViewById(R.id.rd_site_avatar);
            this.rd_photo = (ImageView) convertView.findViewById(R.id.rd_photo);
            this.rd_holder = (LinearLayout) convertView.findViewById(R.id.rd_holder);
            this.rd_topic = (TextView) convertView.findViewById(R.id.rd_topic);
            this.rd_topic_holder = (LinearLayout) convertView.findViewById(R.id.rd_topic_holder);
            //this.rd_add_button = (ImageButton) convertView.findViewById(R.id.rd_add_button);
            //this.rd_share_text = (ImageButton) convertView.findViewById(R.id.rd_share_text);
            this.rd_photo_holder = (LinearLayout) convertView.findViewById(R.id.nd_photo_holder);
            //this.rd_comment_holder = (LinearLayout) convertView.findViewById(R.id.rd_comment_holder);
            //this.rd_user_avatar = (ImageView) convertView.findViewById(R.id.rd_user_avatar);
        }

    }

    public void addItems(List<Post> objects, Integer[] what_first) {
        int before_count = postList.size();

        Integer[] types = {1,3,9};
        ArrayList<Integer> feed_has_img = new ArrayList<Integer>();
        ArrayList<Integer> feed_got_img = new ArrayList<Integer>();

        for (int i=0;i < objects.size();i++) {
            //if (Arrays.asList(types).contains(objects.get(i).getType())){
                //if (objects.get(i).getType() == 1) {
                    if (objects.get(i).getThumbUrl1() != null) {
                        feed_has_img.add(1);
                    } else {
                        feed_has_img.add(0);
                    }
                /*}else {
                    feed_has_img.add(2);
                }*/
            //}
        }

        for (int i=0;i+2 < feed_has_img.size();i++) {
            if (feed_has_img.get(i).intValue() == 1 && feed_has_img.get(i+1).intValue() == 1 && feed_has_img.get(i+2).intValue() == 1){
                feed_got_img.add(i+2);
                feed_has_img.set(i+2,0);
            }
        }

        int t = 0;
        for (int i=0;i+2 < feed_has_img.size();i++) {
            if (t < feed_got_img.size() && feed_has_img.get(i).intValue() == 0 && feed_has_img.get(i+1).intValue() == 0 && feed_has_img.get(i+2).intValue() == 0){
                Collections.swap(objects,feed_got_img.get(t),i+2);
                t++;
            }
        }

        for(int i = 0 ; i<objects.size() ; i++){
            //if (Arrays.asList(types).contains(objects.get(i).getType())){
                postList.add(objects.get(i));
            //}
        }
        for(int i = 0; i< postList.size() ; i++){
            if (/*i<10 && */postList.get(i).getThumbUrl1() != null /*&& postList.get(i).getThumbUrl1().size() > 0*/)
                Picasso.with(context).load(postList.get(i).getThumbUrl1()).fetch();
        }

        int i = before_count;
        while (i < postList.size()){
            /*if (postList.get(i).getType() == 9){
                flip_count.add(1);
                flip_type.add(5);
                flip_data.add(0);
                i = i+1;
            }else if (postList.get(i).getType() == 3){
                flip_count.add(1);
                flip_type.add(6);
                flip_data.add(0);
                i = i+1;
            }else if (postList.get(i).getType() == 1){*/
                if (i+1 == postList.size()/* || (i+1 < postList.size() && postList.get(i+1).getType() != 1)*/){
                    if (postList.get(i).getThumbUrl1() != null) {
                        flip_count.add(1);
                        flip_type.add(4);
                        flip_data.add(0);
                        i = i + 1;
                    }else {
                        flip_count.add(1);
                        flip_type.add(7);
                        flip_data.add(0);
                        i = i + 1;
                    }
                }else if (i+2 == postList.size()/* || (i+2 < postList.size() && postList.get(i+2).getType() != 1)*/){
                    flip_count.add(2);
                    flip_type.add(2);
                    flip_data.add((int)Math.floor(Math.random() * 2));
                    i = i+2;
                }else {
                    int random;
                    if (Arrays.asList(what_first).contains(i))
                        random = 0;
                    else
                        random = (int)Math.floor(Math.random() * 7);

                    if (random == 0 || random == 1){
                        if (Integer.parseInt(postList.get(i).getImageNo()) > 0 || Integer.parseInt(postList.get(i+1).getThumbUrl1()) > 0 || Integer.parseInt(postList.get(i+2).getThumbUrl1()) > 0){
                            flip_count.add(3);
                            flip_type.add(1);
                            flip_data.add((int)Math.floor(Math.random() * 2));
                            i = i+3;
                        }else {
                            random++;
                        }
                    }
                    if (random == 2 || random == 3 || random == 4){
                        flip_count.add(2);
                        flip_type.add(2);
                        flip_data.add((int)Math.floor(Math.random() * 2));
                        i = i+2;
                    }
                    if (random == 5 || random == 6){
                        flip_count.add(3);
                        flip_type.add(3);
                        flip_data.add((int)Math.floor(Math.random() * 2));
                        i = i + 3;
                    }
                    if (random == 7){
                        if (postList.get(i).getThumbUrl1() != null) {
                            flip_count.add(1);
                            flip_type.add(4);
                            flip_data.add(0);
                            i = i + 1;
                        }else {
                            random = random+3;
                        }
                    }
                    if (random == 8 || random == 9){
                        flip_count.add(1);
                        flip_type.add(7);
                        flip_data.add(0);
                        i = i + 1;
                    }
                }
            //}
        }
        notifyDataSetChanged();
    }

    public void addItemsBefore(List<Post> objects) {
        int before_count = objects.size();

        Integer[] types = {1,3,9};
        ArrayList<Integer> feed_has_img = new ArrayList<Integer>();
        ArrayList<Integer> feed_got_img = new ArrayList<Integer>();

        for (int i=0;i < objects.size();i++) {
//            if (Arrays.asList(types).contains(objects.get(i).getType())){
//                if (objects.get(i).getType() == 1) {
                    if (objects.get(i).getThumbUrl1() != null) {
                        feed_has_img.add(1);
                    } else {
                        feed_has_img.add(0);
                    }
//                }else {
//                    feed_has_img.add(2);
//                }
//            }
        }

        for (int i=0;i+2 < feed_has_img.size();i++) {
            if (feed_has_img.get(i).intValue() == 1 && feed_has_img.get(i+1).intValue() == 1 && feed_has_img.get(i+2).intValue() == 1){
                feed_got_img.add(i+2);
                feed_has_img.set(i+2,0);
            }
        }

        int t = 0;
        for (int i=0;i+2 < feed_has_img.size();i++) {
            if (t < feed_got_img.size() && feed_has_img.get(i).intValue() == 0 && feed_has_img.get(i+1).intValue() == 0 && feed_has_img.get(i+2).intValue() == 0){
                Collections.swap(objects,feed_got_img.get(t),i+2);
                t++;
            }
        }

        if (objects.size() > 0) {
            for (int i = objects.size() - 1; i >= 0; i--) {
                //if (Arrays.asList(types).contains(objects.get(i).getType())) {
                    //if (objects.get(i).getThumbUrl1() != null && objects.get(i).getThumbUrl1().size() > 0)
                        //Picasso.with(context).load(objects.get(i).getThumbUrl1()).fetch();
                    postList.add(0, objects.get(i));
                //}
            }
        }

        int i = 0;
        int c = 0;
        while (i < before_count){
            /*if (postList.get(i).getType() == 9){
                flip_count.add(c,1);
                flip_type.add(c,5);
                flip_data.add(c++,0);
                i = i+1;
            }else if (postList.get(i).getType() == 3){
                flip_count.add(c,1);
                flip_type.add(c,6);
                flip_data.add(c++,0);
                i = i+1;
            }else if (postList.get(i).getType() == 1){*/
                if (i+1 == before_count || (i+1 < before_count /*&& postList.get(i+1).getType() != 1*/)){
                    if (postList.get(i).getThumbUrl1() != null) {
                        flip_count.add(c,1);
                        flip_type.add(c,4);
                        flip_data.add(c++,0);
                        i = i + 1;
                    }else {
                        flip_count.add(c,1);
                        flip_type.add(c,7);
                        flip_data.add(c++,0);
                        i = i + 1;
                    }
                }else if (i+2 == before_count || (i+2 < before_count /*&& postList.get(i+2).getType() != 1*/)){
                    flip_count.add(c,2);
                    flip_type.add(c,2);
                    flip_data.add(c++,(int)Math.floor(Math.random() * 2));
                    i = i+2;
                }else {
                    int random;
                    if (i == 0 || i == 1)
                        random = 0;
                    else
                        random = (int)Math.floor(Math.random() * 9);

                    if (random == 0){
                        if (postList.get(i).getThumbUrl1() != null || postList.get(i+1).getThumbUrl1() != null || postList.get(i+2).getThumbUrl1() != null){
                            flip_count.add(c,3);
                            flip_type.add(c,1);
                            flip_data.add(c++,(int)Math.floor(Math.random() * 2));
                            i = i+3;
                        }else {
                            random++;
                        }
                    }
                    if (random == 1){
                        flip_count.add(c,2);
                        flip_type.add(c,2);
                        flip_data.add(c++,(int)Math.floor(Math.random() * 2));
                        i = i+2;
                    }
                    if (random == 2){
                        flip_count.add(c,3);
                        flip_type.add(c,3);
                        flip_data.add(c++,(int)Math.floor(Math.random() * 2));
                        i = i + 3;
                    }
                    if (random >= 3 && random <= 5){
                        if (postList.get(i).getThumbUrl1() != null) {
                            flip_count.add(c,1);
                            flip_type.add(c,4);
                            flip_data.add(c++,0);
                            i = i + 1;
                        }else {
                            random = random+3;
                        }
                    }
                    if (random >= 6 && random <= 8){
                        flip_count.add(c,1);
                        flip_type.add(c,7);
                        flip_data.add(c++,0);
                        i = i + 1;
                    }
                }
            //}
        }

        /*for(int i = 0 ; i<objects.size() ; i++){
            postList.add(0, objects.get(i));
        }*/
        notifyDataSetChanged();
    }

    public void clear(){
        postList = new ArrayList<Post>();
        flip_count = new ArrayList<Integer>();
        flip_type = new ArrayList<Integer>();
        flip_data = new ArrayList<Integer>();
        notifyDataSetChanged();
    }

    public class CircleTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());
            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;
            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }
            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());
            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);
            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);
            squaredBitmap.recycle();
            return bitmap;
        }
        @Override
        public String key() {
            return "circle";
        }
    }
}