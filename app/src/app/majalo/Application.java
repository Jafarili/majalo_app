package app.majalo;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import app.majalo.model.Topic;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import com.onesignal.OneSignal;

public class Application extends android.app.Application {
    private SharedPreferences sharedPreferences;
    private Tracker mTracker;
    public static Map<String, Topic> topics = new HashMap<String, Topic>();

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/IRANSansMobile.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this,Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(false);
        built.setLoggingEnabled(false);
        Picasso.setSingletonInstance(built);
    }

    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // Setting mTracker to Analytics Tracker declared in our xml Folder
            mTracker = analytics.newTracker(R.xml.analytics_tracker);
        }
        return mTracker;
    }

    public SharedPreferences DB() {
        if (sharedPreferences == null)
            sharedPreferences = this.getSharedPreferences("app.majalo.secure.db", Context.MODE_PRIVATE);
        return sharedPreferences;
    }

    public SharedPreferences.Editor DB_editor(){
        SharedPreferences.Editor editor = this.DB().edit();
        return editor;
    }

    public void put(String key, String value){
        this.DB_editor().putString(key, value).commit();
    }

    public void put(String key, int value){
        this.DB_editor().putInt(key, value).commit();
    }

    public List<String> findKeys(String beginning){
        List<String> list = new ArrayList<String>();
        Map<String,?> map = this.DB().getAll();
        for (Map.Entry<String, ?> entry : map.entrySet()) {
            if (entry.getKey().startsWith(beginning))
                list.add(entry.getKey());
        }
        return list;
    }

    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    /*public void share_to_board(final Activity context, Feed feed){
        if (get_token() == null){
            show_login_dialog(context);
            return;
        }else {
            TextView header = new TextView(context);
            header.setText("در کدام بورد منتشر میکنید؟");
            header.setTextSize(16);
            header.setTextColor(Color.parseColor("#666666"));
            header.setShadowLayer(0, 0, 0, Color.parseColor("#f5FFFFFF"));
            header.setGravity(Gravity.RIGHT);
            header.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/IRANSansMobile.ttf"));
            header.setPadding(30, 40, 30, 20);

            View view = LayoutInflater.from(context).inflate(R.layout.dialog_board_select, null, false);
            RecyclerView board_holder = (RecyclerView) view.findViewById(R.id.horizontal_recycler_view);

            board_holder.setHasFixedSize(true);


            LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            board_holder.setLayoutManager(horizontalLayoutManagaer);

            board_holder.setAdapter(adapter);

            DialogPlus dialog = DialogPlus
                    .newDialog(context)
                    .setContentHolder(new com.orhanobut.dialogplus.ViewHolder(view))
                    .setHeader(header)
                    .setExpanded(true, ((int) ((adapter.getElementHeight() / 2.8) * 1.6)) + 205)
                    .setCancelable(true)
                    .setGravity(Gravity.BOTTOM)
                    .create();
            dialog.show();
            adapter.setDialig(dialog);
        }
    }*/

    public View DialogTitle(Context activity , CharSequence text){
        LinearLayout l = new LinearLayout(activity);
        l.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(20,15,20,0);
        l.setLayoutParams(params);
        l.setPadding(30,0,30,0);
        l.setGravity(Gravity.RIGHT);

        TextView t = new TextView(activity);
        t.setText(text);
        //t.setTextDirection(View.TEXT_DIRECTION_RTL);
        t.setGravity(Gravity.RIGHT);
        t.setTextSize(19);
        t.setPadding(30,0,30,0);
        t.setLayoutParams(params);
        t.setTextColor(Color.parseColor("#222222"));
        t.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/IRANSansMobile.ttf"));
        l.addView(t);
        return l;
    }

}
