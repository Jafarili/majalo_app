package app.majalo;

/**
 * Created by ali on 1/14/18.
 */

public class GetTopicPostsRequest {
    final String index;
    final String sort;
    final String code;
    final String token;

    public GetTopicPostsRequest(String index, String sort, String code, String token) {
        this.index = index;
        this.sort = sort;
        this.code = code;
        this.token = token;
    }
}
