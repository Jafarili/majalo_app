package app.majalo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import app.majalo.activity.TopicActivity;
import app.majalo.model.Topic;


public class MainActivityRecyclerViewAdapter extends RecyclerView.Adapter<MainActivityRecyclerViewAdapter.SampleViewHolders>
{
    private List<Topic> itemList;
    private Activity context;

    public MainActivityRecyclerViewAdapter(Activity context, List<Topic> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public SampleViewHolders onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.topic_single_layout, null);
        SampleViewHolders rcv = new SampleViewHolders(layoutView,itemList,context);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final SampleViewHolders holder, final int position)
    {
        final Topic topic = itemList.get(position);
        if (topic.getTitle() != null) {
            holder.title.setText(topic.getTitle());
        }else {
            holder.card.setVisibility(View.GONE);
        }

        if (topic.getThumb_url() != null) {
            Picasso.with(context).load(topic.getThumb_url()).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.trending_gradient_shape).into(holder.thumb_url, new Callback() {
                @Override
                public void onSuccess() {}
                @Override
                public void onError() {
                    Picasso.with(context).load(topic.getThumb_url()).placeholder(R.drawable.trending_gradient_shape).into(holder.thumb_url, new Callback() {
                        @Override
                        public void onSuccess() {}
                        @Override
                        public void onError() {
                            Picasso.with(context).load(R.drawable.ic_widgets_black_36dp).into(holder.thumb_url);
                        }
                    });
                }
            });
        }else {
            holder.card.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount()
    {
        return this.itemList.size();
    }

    static class SampleViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public List<Topic> itemList;
        public CardView card;
        public TextView title;
        public ImageView thumb_url;
        private Activity context;

        public SampleViewHolders(View itemView,List<Topic> itemList, Activity context)
        {
            super(itemView);
            this.context = context;
            itemView.setOnClickListener(this);
            this.itemList = itemList;
            card = (CardView) itemView.findViewById(R.id.card_view);
            title = (TextView) itemView.findViewById(R.id.title);
            thumb_url = (ImageView) itemView.findViewById(R.id.thumb_url);
        }

        @Override
        public void onClick(View view) {
            Topic topic = itemList.get(getPosition());

            Intent intent = new Intent(context, TopicActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("id", topic.getId()+"");
            bundle.putString("title", topic.getTitle());
            intent.putExtras(bundle);
            context.startActivity(intent);
            //context.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
        }
    }
}