package app.majalo.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.afollestad.easyvideoplayer.EasyVideoCallback;
import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.DialogPlusBuilder;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import java.util.List;
import app.majalo.ApiInterface;
import app.majalo.Application;
import app.majalo.GetPostsDetailsRequest;
import app.majalo.R;
import app.majalo.model.Post;
import app.majalo.model.Thumbnail;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class PostViewActivity extends AppCompatActivity {
    private Post post;
    private Activity activity;
    private int post_id;
    private android.support.v4.widget.SwipeRefreshLayout swipeRefreshLayout;
    private TextView title;
    private Tracker mTracker;
    private LinearLayout site_holder;
    private ImageView site_avatar;
    private TextView site_title;
    private TextView publish_date;
    private TextView topic;
    private TextView like_count;
    private ImageButton setting_button;
    //private ImageButton open_button;
    private ImageButton share_text;
    private ApiInterface service;
    public DialogPlusBuilder dialog;
    private Toolbar toolbar;
    public int network_status = 0;
    public boolean FirstFeedGotten = false;
    public boolean FirstFeedLoading = false;
    public boolean getImagesLoading = false;
    private ProgressDialog progress_dialog;
    private SharedPreferences db;
    private SharedPreferences.Editor db_editor;
    private BroadcastReceiver broadcastReceiver;
    public TextView post_textview;
    public ImageView image;
    public WebView browser;
    private SliderLayout mMultiImage;
    private ImageView mSingleImage;
    private EasyVideoPlayer easyVideoPlayer;
    private EasyVideoPlayer easyAudioPlayer;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_view);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Drawable newIcon = getResources().getDrawable(R.drawable.ic_arrow_forward_black_36dp);
        if (newIcon != null) {
            newIcon.mutate().setColorFilter(Color.parseColor("#616161"), PorterDuff.Mode.SRC_IN);
            toolbar.setNavigationIcon(newIcon);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                //overridePendingTransition(R.anim.nothing, R.anim.slide_out);
            }
        });
        toolbar.setTitle("در حال بارگزاری ...");

        broadcastReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                if (null != activeNetwork) {
                    if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                        network_status = 1;
                    if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                        network_status = 2;
                }else {
                    network_status = 0;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Snackbar.make(findViewById(android.R.id.content), "اتصال به اینترنت برقرار نیست.", Snackbar.LENGTH_LONG).show();
                        }
                    }, 1000);
                }
                if (network_status > 0)
                    callForPost();
            }
        };

        registerReceiver(broadcastReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));

        activity = this;
        mTracker = ((Application) getApplication()).getDefaultTracker();
        dialog = DialogPlus.newDialog(this);
        swipeRefreshLayout = (android.support.v4.widget.SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        post_textview = (TextView) findViewById(R.id.post_textview);
        title = (TextView) findViewById(R.id.title);
        site_holder = (LinearLayout) findViewById(R.id.site_holder);
        site_avatar = (ImageView) findViewById(R.id.site_avatar);
        site_title = (TextView) findViewById(R.id.site_title);
        publish_date = (TextView) findViewById(R.id.publish_date);
        topic = (TextView) findViewById(R.id.topic);
        like_count = (TextView) findViewById(R.id.like_count);
        //add_button = (ImageButton) findViewById(R.id.add_button);
        setting_button = (ImageButton) findViewById(R.id.setting_button);
        //open_button = (ImageButton) findViewById(R.id.open_button);
        share_text = (ImageButton) findViewById(R.id.share_text);

        swipeRefreshLayout.setRefreshing(true);

        db = ((Application) getApplication()).DB();
        db_editor = ((Application) getApplication()).DB_editor();

        Bundle bundle = getIntent().getExtras();
        if (bundle.getString("object") != null){
            try {
                post = new Gson().fromJson(bundle.getString("object"),Post.class);
                post_id = post.getID();
                title.setText(post.getTitle());
                if (post.getResTitle() != null && post.getResTitle() != "") {
                    site_holder.setVisibility(View.VISIBLE);
                    site_title.setText(post.getResTitle());
                    toolbar.setTitle(post.getResTitle());
                    if (post.getResource() != null && post.getResource().trim().length() != 0) {
                        site_avatar.setVisibility(View.VISIBLE);
                        Picasso.with(activity).load(post.getResource()).transform(new CircleTransform()).placeholder(R.drawable.trending_gradient_shape).into(site_avatar);
                    }else {
                        site_avatar.setVisibility(View.GONE);
                    }
                }else {
                    site_holder.setVisibility(View.GONE);
                }
                publish_date.setText(post.getPublishTime());

                if (post.getCatTitle() != null && post.getCatTitle() != "") {
                    topic.setVisibility(View.VISIBLE);
                    topic.setText(post.getCatTitle());
                    /*topic.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(activity, TopicActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("id", post.getCategory());
                            bundle.putString("title", post.getCatTitle());
                            intent.putExtras(bundle);
                            activity.startActivity(intent);
                            //activity.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                        }
                    });*/
                }else {
                    topic.setVisibility(View.GONE);
                }
            }catch (IllegalStateException e){
                finish();
            }
        }else if (bundle.getString("id") != null) {
            post_id = bundle.getInt("id");
        }else {
            finish();
            //overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }

        share_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("PostView")
                        .setAction("Share")
                        .build());

                if (activity != null && post != null) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, makeShareString());
                    sendIntent.setType("text/plain");
                    activity.startActivity(sendIntent);
                }
            }
        });
        setting_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("PostView")
                        .setAction("Open Setting")
                        .build());
                View view = LayoutInflater.from(activity).inflate(R.layout.dialog_font_setting, null, false);

                final NumberPicker np = (NumberPicker) view.findViewById(R.id.font_size);
                np.setMaxValue(30);
                np.setMinValue(10);
                np.setValue(16);
                if (db.contains("settings_pageview_font_size")) {
                    np.setValue(db.getInt("settings_pageview_font_size",16));
                }
                np.setWrapSelectorWheel(false);

                final RadioGroup fn = (RadioGroup) view.findViewById(R.id.font_name);
                fn.check(R.id.iransans);
                if (db.contains("settings_pageview_font_name")) {
                    String name = db.getString("settings_pageview_font_name","IRANSansMobile.ttf");
                    if (name.equals("IRANSansMobile.ttf"))
                        fn.check(R.id.iransans);
                    else if (name.equals("b_nazanin.ttf"))
                        fn.check(R.id.nazanin);
                    else if (name.equals("b_zar.ttf"))
                        fn.check(R.id.zar);
                    else if (name.equals("b_yekan.ttf"))
                        fn.check(R.id.yekan);
                    else if (name.equals("danstevis.otf"))
                        fn.check(R.id.dastnevis);
                }

                AlertDialog.Builder builder =  new AlertDialog.Builder(activity);
                builder.setView(view);
                //builder.setTitle("تنظیمات نمایش");
                builder.setCustomTitle(((Application) getApplication()).DialogTitle(activity,"تنظیمات نمایش"));
                builder.setPositiveButton("ثبت", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (np != null)
                            db_editor.putInt("settings_pageview_font_size",np.getValue()).commit();

                        switch (fn.getCheckedRadioButtonId()) {
                            case R.id.iransans:
                                db_editor.putString("settings_pageview_font_name","IRANSansMobile.ttf").commit();
                                break;
                            case R.id.nazanin:
                                db_editor.putString("settings_pageview_font_name","b_nazanin.ttf").commit();
                                break;
                            case R.id.zar:
                                db_editor.putString("settings_pageview_font_name","b_zar.ttf").commit();
                                break;
                            case R.id.yekan:
                                db_editor.putString("settings_pageview_font_name","b_yekan.ttf").commit();
                                break;
                            case R.id.dastnevis:
                                db_editor.putString("settings_pageview_font_name","danstevis.otf").commit();
                                break;
                            default:
                                db_editor.putString("settings_pageview_font_name","IRANSansMobile.ttf").commit();
                                break;
                        }

                        Intent intent = new Intent(activity, PostViewActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("object", new Gson().toJson(post));
                        intent.putExtras(bundle);
                        Runtime.getRuntime().gc();
                        System.gc();
                        activity.finish();
                        //activity.overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                        activity.startActivity(intent);
                        //activity.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                        return;
                    } });
                builder.setNegativeButton("انصراف", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        return;
                    } });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });


        mMultiImage = (SliderLayout)findViewById(R.id.slider);
        mSingleImage = (ImageView) findViewById(R.id.single_image);
        easyVideoPlayer = (EasyVideoPlayer) findViewById(R.id.easyVideoPlayer);
        easyAudioPlayer = (EasyVideoPlayer) findViewById(R.id.easyAudioPlayer);
    }

    public void callForPost(){
        if (!FirstFeedGotten && !FirstFeedLoading){
            progress_dialog = ProgressDialog.show(activity, "", "در حال دریافت اطلاعات...", true);
            progress_dialog.setCancelable(true);
            /*progress_dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    onBackPressed();
                    //overridePendingTransition(R.anim.nothing, R.anim.slide_out);
                }
            });*/
            FirstFeedLoading = true;
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://akharinkhabar.com/Pages/News.aspx/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            service = retrofit.create(ApiInterface.class);
            Call<JsonObject> call = service.getPostDetails(new GetPostsDetailsRequest(post_id));
            call.enqueue(getPostDetailsCallback);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    Call<JsonObject> call = service.getPostDetails(new GetPostsDetailsRequest(post_id));
                    call.enqueue(getPostDetailsCallback);
                }
            });
        }
    }

    public void callForPostImages(){
        if (!getImagesLoading){
            getImagesLoading = true;
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://akharinkhabar.com/Pages/News.aspx/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            service = retrofit.create(ApiInterface.class);
            Call<JsonObject> call = service.getPostGallery(new GetPostsDetailsRequest(post_id));
            call.enqueue(getPostGalleryCallback);
        }
    }

    Callback<JsonObject> getPostDetailsCallback = new Callback<JsonObject>() {
        @Override
        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
            FirstFeedLoading = false;
            try {
                swipeRefreshLayout.setRefreshing(false);
            }catch (NullPointerException e){}
            try{
                if(response.isSuccessful()){

                    List<Post> posts = new Gson().fromJson(response.body().get("d").getAsString(),new TypeToken<List<Post>>(){}.getType());
                    post = posts.get(0);
                    title.setText(post.getTitle());
                    if (Integer.parseInt(post.getImageNo()) > 0)
                        callForPostImages();

                    set_post_images();
                }
            }catch (JsonSyntaxException e){
                Log.e("JsonSyntaxException",e.getMessage(),e);
                set_post_images();
                Snackbar.make(findViewById(android.R.id.content), "متاسفانه مشکلی در دریافت مطالب پیش آمده است.", Snackbar.LENGTH_LONG).show();
            }catch (RuntimeException e){
                Log.e("RuntimeException",e.getMessage(),e);
                set_post_images();
                Snackbar.make(findViewById(android.R.id.content), "متاسفانه مشکلی در دریافت مطالب پیش آمده است.", Snackbar.LENGTH_LONG).show();
            }catch (OutOfMemoryError e){
                Log.e("OutOfMemoryError",e.getMessage(),e);
                set_post_images();
                Snackbar.make(findViewById(android.R.id.content), "متاسفانه مشکلی در دریافت مطالب پیش آمده است.", Snackbar.LENGTH_LONG).show();
            }
            if (progress_dialog != null)
                progress_dialog.dismiss();
        }
        @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {
            Log.e("onFailure","",t);
            FirstFeedLoading = false;
            if (progress_dialog != null)
                progress_dialog.dismiss();

            if (!activity.isFinishing())
                new AlertDialog.Builder(activity)
                        //builder.setTitle("مشکل در برقراری ارتباط!");
                        .setCustomTitle(((Application) getApplication()).DialogTitle(activity,"مشکل در برقراری ارتباط!"))
                        .setMessage("متاسفانه مشکلی در برقراری ارتباط با سرور پیش آمده. دسترسی به اینترنت خود را مجددا بررسی کنید.")
                        .setPositiveButton("بررسی مجدد", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                callForPost();
                            } })
                        .setNegativeButton("انصراف", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                return;
                            } })
                        .create()
                        .show();
            //onBackPressed();
            //overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }
    };

    Callback<JsonObject> getPostGalleryCallback = new Callback<JsonObject>() {
        @Override
        public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
            //getImagesLoading = false;

            try {
                swipeRefreshLayout.setRefreshing(false);
            }catch (NullPointerException e){}
            try{
                if(response.isSuccessful()){
                    final List<Thumbnail> thumbnails = new Gson().fromJson(response.body().get("d").getAsString(),new TypeToken<List<Thumbnail>>(){}.getType());

                    if (post.getIsVocalStream() == 1 && post.getStreamUrl() != null && post.getStreamUrl() != "") {
                        easyAudioPlayer.setVisibility(View.VISIBLE);
                        easyAudioPlayer.setCallback(new EasyVideoCallback() {
                            @Override
                            public void onStarted(EasyVideoPlayer player) {}
                            @Override
                            public void onPaused(EasyVideoPlayer player) {}
                            @Override
                            public void onPreparing(EasyVideoPlayer player) {}
                            @Override
                            public void onPrepared(EasyVideoPlayer player) {}
                            @Override
                            public void onBuffering(int percent) {}
                            @Override
                            public void onError(EasyVideoPlayer player, Exception e) {}
                            @Override
                            public void onCompletion(EasyVideoPlayer player) {}
                            @Override
                            public void onRetry(EasyVideoPlayer player, Uri source) {}
                            @Override
                            public void onSubmit(EasyVideoPlayer player, Uri source) {}
                        });
                        easyAudioPlayer.setSource(Uri.parse(post.getStreamUrl()));
                        easyAudioPlayer.setLeftAction(EasyVideoPlayer.LEFT_ACTION_NONE);
                        easyAudioPlayer.setRightAction(EasyVideoPlayer.RIGHT_ACTION_NONE);
                        easyAudioPlayer.setHideControlsOnPlay(false);
                        easyAudioPlayer.enableControls(true);
                        easyAudioPlayer.setRetryText("تلاش مجدد");
                        easyAudioPlayer.setAutoPlay(true);
                    }

                    if (thumbnails.size() == 1) {
                        if ((post.getIsVideoStream() == 1) && post.getStreamUrl() != null && post.getStreamUrl() != "") {
                            easyVideoPlayer.setVisibility(View.VISIBLE);
                            easyVideoPlayer.setCallback(new EasyVideoCallback() {
                                @Override
                                public void onStarted(EasyVideoPlayer player) {}
                                @Override
                                public void onPaused(EasyVideoPlayer player) {}
                                @Override
                                public void onPreparing(EasyVideoPlayer player) {}
                                @Override
                                public void onPrepared(EasyVideoPlayer player) {}
                                @Override
                                public void onBuffering(int percent) {}
                                @Override
                                public void onError(EasyVideoPlayer player, Exception e) {}
                                @Override
                                public void onCompletion(EasyVideoPlayer player) {}
                                @Override
                                public void onRetry(EasyVideoPlayer player, Uri source) {}
                                @Override
                                public void onSubmit(EasyVideoPlayer player, Uri source) {}
                            });
                            easyVideoPlayer.setSource(Uri.parse(post.getStreamUrl()));
                            easyVideoPlayer.setLeftAction(EasyVideoPlayer.LEFT_ACTION_NONE);
                            easyVideoPlayer.setRightAction(EasyVideoPlayer.RIGHT_ACTION_NONE);
                            easyVideoPlayer.setHideControlsOnPlay(true);
                            easyVideoPlayer.setRetryText("تلاش مجدد");
                            easyVideoPlayer.setAutoPlay(true);
                        } else {
                            mSingleImage.setVisibility(View.VISIBLE);
                            mSingleImage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(activity, GalleryActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("images", new Gson().toJson(thumbnails));
                                    //bundle.putString("id", st_data.getId());
                                    intent.putExtras(bundle);
                                    activity.startActivity(intent);
                                    //activity.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                                }
                            });
                            Picasso.with(activity).load(thumbnails.get(0).getThumbUrl()).networkPolicy(NetworkPolicy.OFFLINE).placeholder(R.drawable.trending_gradient_shape).fit().into(mSingleImage, new com.squareup.picasso.Callback() {
                                @Override
                                public void onSuccess() {}
                                @Override
                                public void onError() {
                                    Picasso.with(activity).load(thumbnails.get(0).getThumbUrl()).placeholder(R.drawable.trending_gradient_shape).fit().into(mSingleImage);
                                }
                            });
                        }
                    }else if (thumbnails.size() > 1){
                        mMultiImage.setVisibility(View.VISIBLE);
                        for(int i=0;i<thumbnails.size();i++){
                            TextSliderView textSliderView = new TextSliderView(activity);
                            textSliderView
                                    //.description(name)
                                    .image(thumbnails.get(i).getThumbUrl())
                                    .setScaleType(BaseSliderView.ScaleType.Fit)
                                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                        @Override
                                        public void onSliderClick(BaseSliderView slider) {
                                            Intent intent = new Intent(activity, GalleryActivity.class);
                                            Bundle bundle = new Bundle();
                                            bundle.putString("images", new Gson().toJson(thumbnails));
                                            bundle.putString("index", slider.getBundle().getInt("index")+"");
                                            intent.putExtras(bundle);
                                            activity.startActivity(intent);
                                            //activity.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                                        }
                                    });

                            //add your extra information
                            textSliderView.bundle(new Bundle());
                            textSliderView.getBundle().putInt("index",i);

                            mMultiImage.addSlider(textSliderView);
                        }
                        mMultiImage.setPresetTransformer(SliderLayout.Transformer.Default);
                        mMultiImage.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                        mMultiImage.setCustomAnimation(new DescriptionAnimation());
                        mMultiImage.setDuration(4000);
                        //mMultiImage.stopAutoCycle();
                        mMultiImage.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
                            @Override
                            public void onPageSelected(int position) {
                                //Log.d("Slider_Demo", "Page Changed: " + position);
                            }
                            @Override
                            public void onPageScrollStateChanged(int state) {
                                Log.d("Slider_Demo", "onPageScrollStateChanged: " + state);
                                if (state == 0)
                                    swipeRefreshLayout.setEnabled(true);
                                else
                                    swipeRefreshLayout.setEnabled(false);
                            }
                        });
                    }
                }
            }catch (JsonSyntaxException e){
                Log.e("JsonSyntaxException",e.getMessage(),e);
                Snackbar.make(findViewById(android.R.id.content), "متاسفانه مشکلی در دریافت مطالب پیش آمده است.", Snackbar.LENGTH_LONG).show();
            }catch (RuntimeException e){
                Log.e("RuntimeException",e.getMessage(),e);
                Snackbar.make(findViewById(android.R.id.content), "متاسفانه مشکلی در دریافت مطالب پیش آمده است.", Snackbar.LENGTH_LONG).show();
            }catch (OutOfMemoryError e){
                Log.e("OutOfMemoryError",e.getMessage(),e);
                Snackbar.make(findViewById(android.R.id.content), "متاسفانه مشکلی در دریافت مطالب پیش آمده است.", Snackbar.LENGTH_LONG).show();
            }
            if (progress_dialog != null)
                progress_dialog.dismiss();
        }
        @Override
        public void onFailure(Call<JsonObject> call, Throwable t) {
            Log.e("onFailure","",t);
            getImagesLoading = false;
            if (progress_dialog != null)
                progress_dialog.dismiss();

            if (!activity.isFinishing())
                new AlertDialog.Builder(activity)
                        //builder.setTitle("مشکل در برقراری ارتباط!");
                        .setCustomTitle(((Application) getApplication()).DialogTitle(activity,"مشکل در برقراری ارتباط!"))
                        .setMessage("متاسفانه مشکلی در برقراری ارتباط با سرور پیش آمده. دسترسی به اینترنت خود را مجددا بررسی کنید.")
                        .setPositiveButton("بررسی مجدد", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                callForPostImages();
                            } })
                        .setNegativeButton("انصراف", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                return;
                            } })
                        .create()
                        .show();
            //onBackPressed();
            //overridePendingTransition(R.anim.nothing, R.anim.slide_out);
        }
    };

    public void set_post_images(){
        int cut = post.getBody().indexOf("همراهان عزیز، آخرین خبر");
        if (cut > 10)
            post.setBody(post.getBody().substring(0,cut));

        post_textview.setTextIsSelectable(true);
        post_textview.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/IRANSansMobile.ttf"));
        try {
            if (db.contains("settings_pageview_font_name")) {
                post_textview.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/" + db.getString("settings_pageview_font_name", "IRANSansMobile.ttf")));
            }
        } catch (RuntimeException e) {}

        post_textview.setTextSize(16);
        if (db.contains("settings_pageview_font_size")) {
            post_textview.setTextSize(db.getInt("settings_pageview_font_size", 16));
        }

        post_textview.setText(String.valueOf(Html.fromHtml(post.getBody())));

        post_textview.setTextColor(Color.parseColor("#333333"));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
            post_textview.setTextDirection(View.TEXT_DIRECTION_FIRST_STRONG);
        post_textview.setLineSpacing(1f,1.3f);

        if (post.getResTitle() != null && post.getResTitle() != "") {
            site_title.setText(post.getResTitle());
            toolbar.setTitle(post.getResTitle());
            if (post.getResource() != null && post.getResource().trim().length() != 0) {
                site_avatar.setVisibility(View.VISIBLE);
                Picasso.with(activity).load(post.getResource()).transform(new CircleTransform()).placeholder(R.drawable.trending_gradient_shape).into(site_avatar);
            }else {
                site_avatar.setVisibility(View.GONE);
            }
        }
        publish_date.setText(post.getPublishTime());

        if (post.getCatTitle() != null && post.getCatTitle() != "") {
            topic.setVisibility(View.VISIBLE);
            topic.setText(post.getCatTitle());
            /*topic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, TopicActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("id", post.getCategory());
                    bundle.putString("title", post.getCatTitle());
                    intent.putExtras(bundle);
                    activity.startActivity(intent);
                    //activity.overridePendingTransition(R.anim.slide_in, R.anim.nothing);
                }
            });*/
        }else {
            topic.setVisibility(View.GONE);
        }
        like_count.setText(String.valueOf(post.getVisitNo()));

        FirstFeedGotten = true;
    }

    public String makeShareString() {
        return (post.getResTitle() != null && post.getResTitle() != "" ? post.getResTitle() + ":\n" : "") + post.getTitle() + "\n\n" + Html.fromHtml(post.getBody()) + "\n\n\n" + "دریافت آخرین اخبار و مطالب مفید با اپلیکیشن مَجَلو";
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    public class CircleTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());
            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;
            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }
            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());
            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);
            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);
            squaredBitmap.recycle();
            return bitmap;
        }
        @Override
        public String key() {
            return "circle";
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Post View Screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        mMultiImage.stopAutoCycle();
        easyVideoPlayer.pause();
        easyAudioPlayer.pause();
        super.onStop();
    }

    @Override
    public void onPause() {
        easyVideoPlayer.pause();
        easyAudioPlayer.pause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        /*post = null;
        swipeRefreshLayout = null;
        progress_dialog = null;
        db = null;
        db_editor = null;
        service = null;
        dialog = null;*/
        if (browser != null)
            browser.destroy();
        //browser = null;
        //comment_holder = null;
        unregisterReceiver(broadcastReceiver);
        super.onDestroy();
        Runtime.getRuntime().gc();
        System.gc();
        finish();
    }
}