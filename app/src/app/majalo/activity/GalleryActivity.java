package app.majalo.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.ImageView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.veinhorn.scrollgalleryview.MediaInfo;
import com.veinhorn.scrollgalleryview.ScrollGalleryView;
import com.veinhorn.scrollgalleryview.loader.MediaLoader;
import java.util.List;
import app.majalo.R;
import app.majalo.model.Thumbnail;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class GalleryActivity extends FragmentActivity {

    private ScrollGalleryView scrollGalleryView;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        Bundle bundle = getIntent().getExtras();

        scrollGalleryView = (ScrollGalleryView) findViewById(R.id.scroll_gallery_view);
        scrollGalleryView
                .setThumbnailSize(200)
                .setZoom(true)
                .setFragmentManager(getSupportFragmentManager());
                //.addMedia(MediaInfo.mediaLoader(new DefaultVideoLoader(movieUrl, R.drawable.default_video)));

        if (bundle.getString("images") != null) {
            List<Thumbnail> images = new Gson().fromJson(bundle.getString("images"),new TypeToken<List<Thumbnail>>(){}.getType());
            Log.e("dsfsdffs",images.get(0).getThumbUrl());
            for (Thumbnail image : images)
                scrollGalleryView.addMedia(MediaInfo.mediaLoader(new PicassoImageLoader(image.getThumbUrl())));
        }
        if (bundle.getString("index") != null) {
            scrollGalleryView.setCurrentItem(Integer.valueOf(bundle.getString("index")));
        }

    }

    public class PicassoImageLoader implements MediaLoader {
        private String url;
        public PicassoImageLoader(String url) {
            this.url = url;
        }
        @Override
        public boolean isImage() {
            return true;
        }
        @Override
        public void loadMedia(Context context, final ImageView imageView, final MediaLoader.SuccessCallback callback) {
            Picasso.with(context)
                    .load(url)
                    .placeholder(R.drawable.placeholder_image)
                    .into(imageView, new ImageCallback(callback));
        }
        @Override
        public void loadThumbnail(Context context, ImageView thumbnailView, MediaLoader.SuccessCallback callback) {
            Picasso.with(context)
                    .load(url)
                    .resize(100, 100)
                    .placeholder(R.drawable.placeholder_image)
                    .centerInside()
                    .into(thumbnailView, new ImageCallback(callback));
        }
        private class ImageCallback implements Callback {
            private final MediaLoader.SuccessCallback callback;
            public ImageCallback(SuccessCallback callback) {
                this.callback = callback;
            }
            @Override
            public void onSuccess() {
                callback.onSuccess();
            }
            @Override
            public void onError() {}
        }
    }
}
