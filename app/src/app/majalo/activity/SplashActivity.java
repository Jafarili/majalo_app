package app.majalo.activity;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import app.majalo.ApiInterface;
import app.majalo.DatabaseHandler;
import app.majalo.R;
import app.majalo.model.Topic;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static app.majalo.Application.topics;
import static app.majalo.DatabaseHandler.TABLE_TOPICS;

public class SplashActivity extends AppCompatActivity {

    public Activity activity;
    private SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        activity = this;

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, 3000);

        DatabaseHandler databaseHandler = new DatabaseHandler(this);
        db = databaseHandler.getWritableDatabase();

        Cursor cursor = db.rawQuery("SELECT id FROM " + TABLE_TOPICS, null);
        if (cursor.getCount() == 0) {
            List<Topic> default_topic_list = new ArrayList<Topic>();
            default_topic_list.add(new Topic(0,"اخبار برگزیده",1,"http://app.akharinkhabar.ir/AndroidOnlineNewsImage.aspx?id=14441197&q=cat"));
            default_topic_list.add(new Topic(-1,"همه اخبار",2,"http://app.akharinkhabar.ir/AndroidOnlineNewsImage.aspx?id=13870271&q=cat"));
            default_topic_list.add(new Topic(2,"سياسی",3,"http://app.akharinkhabar.ir/AndroidOnlineNewsImage.aspx?id=14441031&q=cat"));
            default_topic_list.add(new Topic(3,"ورزشی",4,"http://app.akharinkhabar.ir/AndroidOnlineNewsImage.aspx?id=14441202&q=cat"));
            default_topic_list.add(new Topic(1,"اقتصادی",5,"http://app.akharinkhabar.ir/AndroidOnlineNewsImage.aspx?id=14441257&q=cat"));
            default_topic_list.add(new Topic(220,"جذاب ترين ها",6,"http://app.akharinkhabar.ir/AndroidOnlineNewsImage.aspx?id=14440985&q=cat"));
            default_topic_list.add(new Topic(8,"فناوری اطلاعات",7,"http://app.akharinkhabar.ir/AndroidOnlineNewsImage.aspx?id=14441027&q=cat"));
            default_topic_list.add(new Topic(43,"چند رسانه ای",8,"http://app.akharinkhabar.ir/AndroidOnlineNewsImage.aspx?id=14441008&q=cat"));
            default_topic_list.add(new Topic(11,"بين الملل",9,"http://app.akharinkhabar.ir/AndroidOnlineNewsImage.aspx?id=14441188&q=cat"));
            default_topic_list.add(new Topic(19,"جامعه و حوادث",10,"http://app.akharinkhabar.ir/AndroidOnlineNewsImage.aspx?id=14440829&q=cat"));
            default_topic_list.add(new Topic(30,"تحليل ها",11,"http://app.akharinkhabar.ir/AndroidOnlineNewsImage.aspx?id=14440873&q=cat"));
            default_topic_list.add(new Topic(18,"سينما و چهره ها",12,"http://app.akharinkhabar.ir/AndroidOnlineNewsImage.aspx?id=14441277&q=cat"));
            default_topic_list.add(new Topic(32,"خودرو",13,"http://app.akharinkhabar.ir/AndroidOnlineNewsImage.aspx?id=14437998&q=cat"));
            default_topic_list.add(new Topic(22,"خواندنی ها",14,"http://app.akharinkhabar.ir/AndroidOnlineNewsImage.aspx?id=14441269&q=cat"));
            default_topic_list.add(new Topic(27,"دخترونه  زنونه",15,"http://app.akharinkhabar.ir/AndroidOnlineNewsImage.aspx?id=14440998&q=cat"));
            default_topic_list.add(new Topic(29,"سلامت",16,"http://app.akharinkhabar.ir/AndroidOnlineNewsImage.aspx?id=14441182&q=cat"));
            default_topic_list.add(new Topic(42,"کتاب",17,"http://app.akharinkhabar.ir/AndroidOnlineNewsImage.aspx?id=14438894&q=cat"));
            default_topic_list.add(new Topic(35,"علمی",18,"http://app.akharinkhabar.ir/AndroidOnlineNewsImage.aspx?id=14441195&q=cat"));

            for (int i=0;i<default_topic_list.size();i++) {
                Topic topic = default_topic_list.get(i);
                ContentValues values = new ContentValues();
                values.put("id", topic.getId());
                values.put("title", topic.getTitle());
                values.put("position", topic.getOrder());
                values.put("thumb_url", topic.getThumb_url());

                // Inserting Row
                db.insert(TABLE_TOPICS, null, values);
                topics.put(String.valueOf(topic.getId()),topic);
                Log.e("insert topic "+topic.getId(),topic.getTitle());
            }
        }


        if (true) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://majalo.ml/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            ApiInterface service = retrofit.create(ApiInterface.class);

            Call<List<Topic>> call = service.getTopicList();
            call.enqueue(new retrofit2.Callback<List<Topic>>() {
                @Override
                public void onResponse(Call<List<Topic>> call, retrofit2.Response<List<Topic>> r) {
                    if(r.isSuccessful()) {
                        for (int i=0;i<r.body().size();i++) {
                            Topic topic = r.body().get(i);
                            try {
                                ContentValues values = new ContentValues();
                                values.put("thumb_url", topic.getThumb_url());
                                // updating row
                                int update_query_response = db.update(TABLE_TOPICS, values, "id = ?", new String[] { String.valueOf(topic.getId()) });
                                if (update_query_response == 1)
                                    Picasso.with(activity).load(topic.getThumb_url()).fetch();
                            }catch (NullPointerException e) {}
                        }
                    }
                }
                @Override
                public void onFailure(Call<List<Topic>> call, Throwable t) {}
            });
        }
    }
}
