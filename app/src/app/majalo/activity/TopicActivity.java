package app.majalo.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import java.util.List;
import app.majalo.ApiInterface;
import app.majalo.Application;
import app.majalo.DatabaseHandler;
import app.majalo.GetTopicPostsRequest;
import app.majalo.FlipAdapter;
import app.majalo.R;
import app.majalo.model.Post;
import app.majalo.model.Topic;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import se.emilsjolander.flipview.FlipView;
import se.emilsjolander.flipview.OverFlipMode;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static app.majalo.DatabaseHandler.TABLE_TOPICS;

public class TopicActivity extends AppCompatActivity {
    public Activity activity;
    private FlipView mFlipView;
    private FlipAdapter mAdapter;
    public int load_index = 0;
    boolean loading = true;
    public boolean FirstFeedGotten = false;
    public boolean FirstFeedLoading = false;
    private Toolbar toolbar;
    private Topic topic;
    private TextView toolbar_title;
    private ProgressDialog progress_dialog;
    private Tracker mTracker;
    private SharedPreferences shared_db;
    private SharedPreferences.Editor db_editor;
    private SQLiteDatabase db;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic);
        activity = this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        Drawable newIcon = getResources().getDrawable(R.drawable.ic_arrow_forward_black_36dp);
        if (newIcon != null) {
            newIcon.mutate().setColorFilter(Color.parseColor("#616161"), PorterDuff.Mode.SRC_IN);
            toolbar.setNavigationIcon(newIcon);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                //overridePendingTransition(R.anim.nothing, R.anim.slide_out);
            }
        });
        toolbar_title = (TextView) findViewById(R.id.toolbar_title);
        /*follow_button = (TextView) findViewById(R.id.follow);
        unfollow_button = (TextView) findViewById(R.id.unfollow);*/

        mTracker = ((Application) getApplication()).getDefaultTracker();
        shared_db = ((Application) activity.getApplication()).DB();
        db_editor = ((Application) activity.getApplication()).DB_editor();

        DatabaseHandler databaseHandler = new DatabaseHandler(this);
        db = databaseHandler.getWritableDatabase();

        Bundle bundle = getIntent().getExtras();
        if (bundle.getString("title") != null)
            toolbar_title.setText(bundle.getString("title"));

        if (bundle.getString("id") != null){
            Cursor cursor = db.query(TABLE_TOPICS, new String[] { "id", "title", "position", "thumb_url" }, "id=?", new String[] { bundle.getString("id") }, null, null, null, "1");
            if (cursor != null && cursor.getCount() == 1) {
                cursor.moveToFirst();
                topic = new Topic(Integer.parseInt(cursor.getString(0)), cursor.getString(1), Integer.parseInt(cursor.getString(2)), cursor.getString(3));
            }
        }

        if (topic == null) {
            Toast.makeText(activity,"چنین موضوعی وجود ندارد!",Toast.LENGTH_SHORT).show();
            finish();
            //overridePendingTransition(R.anim.nothing, R.anim.slide_out);
            return;
        }

        toolbar_title.setText(topic.getTitle());

        progress_dialog = ProgressDialog.show(activity, "", "در حال دریافت اطلاعات...", true);
        progress_dialog.setCancelable(true);
        progress_dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                onBackPressed();
                //overridePendingTransition(R.anim.nothing, R.anim.slide_out);
            }
        });

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width=dm.widthPixels;
        int height=dm.heightPixels;
        int dens=dm.densityDpi;
        double wi=(double)width/(double)dens;
        double hi=(double)height/(double)dens;
        double x = Math.pow(wi,2);
        double y = Math.pow(hi,2);
        double screenInches = Math.sqrt(x+y);

        mFlipView = (FlipView) findViewById(R.id.flip_view);
        mFlipView.setFlippingVertically(true);

        mAdapter = new FlipAdapter(mFlipView,activity,screenInches/*,mItems*/);
        mAdapter.setTopic(topic);
        mFlipView.setAdapter(mAdapter);

        toolbar_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFlipView.smoothFlipTo(0);
            }
        });

        //getInfo();
        getFirstFeeds();

        mFlipView.setOnFlipListener(new FlipView.OnFlipListener() {
            @Override
            public void onFlippedToPage(FlipView v,final int position, long id) {
                if(position > mFlipView.getPageCount()-7){
                    if (loading){
                        loading = false;
                        Retrofit retrofit = new Retrofit.Builder()
                                .baseUrl("http://akharinkhabar.com/Pages/NewsList.aspx/")
                                .addConverterFactory(GsonConverterFactory.create())
                                .build();
                        ApiInterface service = retrofit.create(ApiInterface.class);
                        Call<JsonObject> call2 = service.getTopicPosts(new GetTopicPostsRequest(load_index+"", "0", String.valueOf(topic.getId()), ""));
                        call2.enqueue(new retrofit2.Callback<JsonObject>() {
                            @Override
                            public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> r) {
                                if(r.isSuccessful()) {
                                    load_index++;
                                    List<Post> posts = new Gson().fromJson(r.body().get("d").getAsString(),new TypeToken<List<Post>>(){}.getType());
                                    for (int i=0;i<posts.size();i++)
                                        if (Integer.parseInt(posts.get(i).getIsAdver()) == 1)
                                            posts.remove(i);

                                    if (posts != null && posts.size() > 0){
                                        Integer[] what_first = {0};
                                        mAdapter.addItems(posts,what_first);
                                    }
                                    /*if (r.body().get("code").getAsInt() == 200){
                                        List<Post> mItems = new ArrayList<Post>();
                                        if (r.body().get("load_index") != null && !r.body().get("load_index").isJsonNull())
                                            load_index = r.body().get("load_index").getAsInt();

                                        if (r.body().get("feeds") != null && !r.body().get("feeds").isJsonNull()) {
                                            JsonArray feeds = r.body().get("feeds").getAsJsonArray();
                                            for (int i = 0; i < feeds.size(); i++) {
                                                mItems.add(i, new Gson().fromJson(feeds.get(i).getAsJsonObject(), Post.class));
                                            }
                                            Integer[] what_first = {0};
                                            mAdapter.addItems(mItems,what_first);
                                        }else {
                                            Snackbar.make(findViewById(android.R.id.content), "مشکلی در دریافت اطلاعات پیش آمده!", Snackbar.LENGTH_LONG).show();
                                        }
                                    }else if (r.body().has("error_id")){
                                        String error = r.body().get("error_id").getAsString();
                                        if (error.contains("no_more_feed")){
                                            if (mAdapter.getCount() == 0){
                                                //TODO show empty flip
                                            }else if(position >= mFlipView.getPageCount()-1) {
                                                Snackbar.make(findViewById(android.R.id.content), "مطلب بیشتری وجود ندارد.", Snackbar.LENGTH_LONG).show();
                                            }
                                        } else if (error.contains("no_id") || error.contains("invalid_type") || error.contains("no_topic")) {
                                            Snackbar.make(findViewById(android.R.id.content), "چنین موضوعی وجود ندارد!", Snackbar.LENGTH_LONG).show();
                                        }
                                    }*/
                                }else {
                                    Toast.makeText(getApplicationContext(),"مشکلی در سمت سرور پیش آمده است!",Toast.LENGTH_SHORT).show();
                                }
                                loading = true;
                            }
                            @Override
                            public void onFailure(Call<JsonObject> call, Throwable t) {
                                loading = true;
                                if ((((Application) getApplication()).isNetworkConnected()))
                                    Snackbar.make(findViewById(android.R.id.content), "مشکلی در دریافت اطلاعات پیش آمده!", Snackbar.LENGTH_LONG).show();
                                else
                                    Snackbar.make(findViewById(android.R.id.content), "اتصال به اینترنت لازم است.", Snackbar.LENGTH_LONG).show();
                            }
                        });
                    }
                }
            }
        });
        mFlipView.setOverFlipMode(OverFlipMode.RUBBER_BAND);
        mFlipView.setEmptyView(findViewById(R.id.empty_view));
        /*mFlipView.setOnOverFlipListener(new FlipView.OnOverFlipListener() {
            @Override
            public void onOverFlip(FlipView v, OverFlipMode mode, boolean overFlippingPrevious, float overFlipDistance, float flipDistancePerPage) {
                //Log.i("overflip", "overFlippingPrevious = "+overFlippingPrevious+" | overFlipDistance = "+overFlipDistance+" | mode"+mode.name());
            }
        });*/
        mFlipView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });
    }

    public void getFirstFeeds(){
        if (!FirstFeedGotten && !FirstFeedLoading){
            FirstFeedLoading = true;
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://akharinkhabar.com/Pages/NewsList.aspx/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            ApiInterface service = retrofit.create(ApiInterface.class);
            Call<JsonObject> call = service.getTopicPosts(new GetTopicPostsRequest(load_index+"", "0", String.valueOf(topic.getId()), ""));
            call.enqueue(new retrofit2.Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, retrofit2.Response<JsonObject> r) {
                    if(r.isSuccessful()) {
                        List<Post> posts = new Gson().fromJson(r.body().get("d").getAsString(),new TypeToken<List<Post>>(){}.getType());
                        for (int i=0;i<posts.size();i++)
                            if (Integer.parseInt(posts.get(i).getIsAdver()) == 1)
                                posts.remove(i);

                        if (posts != null && posts.size() > 0){
                            mAdapter.clear();
                            Integer[] what_first = {0};
                            mAdapter.addItems(posts,what_first);
                            for (int i = mAdapter.getCount() - 1; i >= 0; i--)
                                mFlipView.flipTo(i);
                            FirstFeedGotten = true;

                            int first_flips = shared_db.getInt("first_flips",0);
                            if (first_flips < 3) {
                                mFlipView.peakNext(false);
                                db_editor.putInt("first_flips",++first_flips).commit();
                            }
                        }
/*
                        if (r.body().get("code").getAsInt() == 200) {
                            List<Feed> mItems = new ArrayList<Feed>();
                            if (r.body().get("load_index") != null && !r.body().get("load_index").isJsonNull())
                                load_index = r.body().get("load_index").getAsInt();
                            if (r.body().get("first_id") != null && !r.body().get("first_id").isJsonNull())
                                first_id = r.body().get("first_id").getAsInt();

                            if (r.body().get("feeds") != null && !r.body().get("feeds").isJsonNull()) {
                                JsonArray feeds = r.body().get("feeds").getAsJsonArray();
                                for (int i = 0; i < feeds.size(); i++) {
                                    try {
                                        mItems.add(new Gson().fromJson(feeds.get(i).getAsJsonObject(), Feed.class));
                                    }catch (JsonSyntaxException e){
                                        Log.e("hgjgjdghj",feeds.get(i).toString());
                                        Log.e("hgjgjdghj",e.getMessage());
                                    }
                                }
                                mAdapter.clear();
                                Integer[] what_first = {0};
                                mAdapter.addItems(mItems,what_first);
                                for (int i = mAdapter.getCount() - 1; i >= 0; i--)
                                    mFlipView.flipTo(i);
                                FirstFeedGotten = true;
                            } else {
                                Snackbar.make(findViewById(android.R.id.content), "مشکلی در دریافت اطلاعات پیش آمده!", Snackbar.LENGTH_LONG).show();
                            }
                        }else if (r.body().has("error_id")){
                            String error = r.body().get("error_id").getAsString();
                            if (error.contains("no_more_feed")){
                                Snackbar.make(findViewById(android.R.id.content), "متاسفانه مطلبی وجود ندارد!", Snackbar.LENGTH_LONG).show();
                            } else if (error.contains("no_id") || error.contains("invalid_type") || error.contains("no_topic")) {
                                Snackbar.make(findViewById(android.R.id.content), "چنین موضوعی وجود ندارد!", Snackbar.LENGTH_LONG).show();
                            }
                        }
*/
                    }else {
                        Toast.makeText(getApplicationContext(),r.message(),Toast.LENGTH_SHORT).show();
                    }
                    if (progress_dialog != null)
                        progress_dialog.dismiss();
                    FirstFeedLoading = false;
                }
                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    FirstFeedLoading = false;
                    if (progress_dialog != null)
                        progress_dialog.dismiss();
                    Log.e("topic_get_feed","onFailure",t);
                    if ((((Application) getApplication()).isNetworkConnected()))
                        Snackbar.make(findViewById(android.R.id.content), "مشکلی در دریافت اطلاعات پیش آمده!", Snackbar.LENGTH_LONG).show();
                    else
                        Snackbar.make(findViewById(android.R.id.content), "اتصال به اینترنت لازم است.", Snackbar.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //overridePendingTransition(R.anim.nothing, R.anim.slide_out);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.topic_view_menu, menu);
        for (int i=0; i < menu.size();i++){
            MenuItem favoriteItem = menu.getItem(i);
            Drawable newIcon = favoriteItem.getIcon();
            if (newIcon != null) {
                newIcon.mutate().setColorFilter(Color.parseColor("#616161"), PorterDuff.Mode.SRC_IN);
                favoriteItem.setIcon(newIcon);
            }
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*if(item.getItemId() == R.id.open_browser){

        }*/
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        mTracker.setScreenName("Topic View Screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
