package app.majalo.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.List;
import app.majalo.ApiInterface;
import app.majalo.Application;
import app.majalo.DatabaseHandler;
import app.majalo.MainActivityRecyclerViewAdapter;
import app.majalo.R;
import app.majalo.model.Topic;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import static app.majalo.Application.topics;
import static app.majalo.DatabaseHandler.TABLE_TOPICS;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout mDrawer;
    private NavigationView nvDrawer;
    private FragmentActivity activity;
    private Tracker mTracker;
    private SharedPreferences shared_db;
    private SharedPreferences.Editor db_editor;
    private StaggeredGridLayoutManager _sGridLayoutManager;
    private SQLiteDatabase db;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.setDrawerListener(toggle);
        toggle.syncState();

        nvDrawer = (NavigationView) findViewById(R.id.nav_view);
        nvDrawer.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        mDrawer.closeDrawers();

                        switch(menuItem.getItemId()) {
                            case R.id.contact_us:
                                /*mTracker.send(new HitBuilders.EventBuilder()
                                        .setCategory("TopicOption")
                                        .setAction("contact_us")
                                        .build());*/

                                LinearLayout linearLayout = new LinearLayout(activity);
                                linearLayout.setOrientation(LinearLayout.VERTICAL);
                                linearLayout.setPadding(40,15,40,15);

                                TextView text = new TextView(activity);
                                text.setText("میتوانید از طریق فرم زیر با ما ارتباط برقرار کنید:");
                                text.setPadding(0,0,0,20);
                                text.setTextSize(17);
                                text.setTextColor(Color.parseColor("#333333"));
                                text.setTextIsSelectable(true);

                                final EditText msg = new EditText(activity);
                                msg.setMaxLines(10);
                                msg.setLines(5);
                                msg.setTextSize(15);
                                msg.setHint("متن پیام:");

                                final EditText phone = new EditText(activity);
                                phone.setSingleLine(true);
                                phone.setTextSize(13);
                                phone.setHint("شماره موبایل یا ID تلگرام (اختیاری):");

                                linearLayout.addView(text);
                                linearLayout.addView(msg);
                                linearLayout.addView(phone);

                                final AlertDialog alertDialog = new AlertDialog.Builder(activity)
                                        .setCustomTitle(((Application) activity.getApplication()).DialogTitle(activity,"ارتباط با ما"))
                                        .setView(linearLayout)
                                        .setPositiveButton("ارسال", null)
                                        .setNegativeButton("بی خیال!", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        })
                                        .create();

                                alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                                    @Override
                                    public void onShow(DialogInterface d) {
                                        Button positive = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                                        positive.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                if (msg.getText().toString().isEmpty()) {
                                                    msg.setError("این فیلد نمیتواند خالی باشد.");
                                                    return;
                                                }
                                                final ProgressDialog progress_dialog = ProgressDialog.show(activity, "", "در حال ارسال...", true);
                                                progress_dialog.show();
                                                Retrofit retrofit = new Retrofit.Builder()
                                                        .baseUrl("https://majalo.ml/")
                                                        .addConverterFactory(GsonConverterFactory.create())
                                                        .build();
                                                ApiInterface service = retrofit.create(ApiInterface.class);
                                                Call<JsonObject> call = service.submitFeedback("\n\n"+msg.getText().toString(), phone.getText().toString());
                                                call.enqueue(new Callback<JsonObject>() {
                                                    @Override
                                                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                                                        if(response.isSuccessful() && response.body().get("code").getAsInt() == 200) {
                                                            progress_dialog.dismiss();
                                                            alertDialog.dismiss();
                                                            Toast.makeText(activity, "پیام شما با موفقیت دریافت شد.", Toast.LENGTH_LONG).show();
                                                        }else {
                                                            progress_dialog.dismiss();
                                                            Toast.makeText(activity, "مشکلی پیش آمده! مجددا امتحان کنید.", Toast.LENGTH_LONG).show();
                                                        }
                                                    }
                                                    @Override
                                                    public void onFailure(Call<JsonObject> call, Throwable t) {
                                                        progress_dialog.dismiss();
                                                        Toast.makeText(activity, "مشکلی پیش آمده! مجددا امتحان کنید.", Toast.LENGTH_LONG).show();
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });

                                alertDialog.show();
                                break;
                            case R.id.invite_people:
                                /*mTracker.send(new HitBuilders.EventBuilder()
                                        .setCategory("TopicOption")
                                        .setAction("invite")
                                        .build());*/
                                /*db.execSQL("delete from "+TABLE_TOPICS+"");
                                db_editor.putInt("first_flips",0).commit();*/
                                Intent sendIntent = new Intent();
                                sendIntent.setAction(Intent.ACTION_SEND);
                                sendIntent.putExtra(Intent.EXTRA_TEXT, "مَجَلو یک اپلیکیشن برای خواندن اخبار و مطالبه.\nمیتوانی این اپلیکیشن را از کافه بازار دانلود کنی:\nhttps://cafebazaar.ir/app/app.majalo/");
                                sendIntent.setType("text/plain");
                                activity.startActivity(sendIntent);
                                break;
                            case R.id.caffebazzar_comment:
                                if (isPackageInstalled("com.farsitel.bazaar",getPackageManager())) {
                                    Intent intent1 = new Intent(Intent.ACTION_EDIT);
                                    intent1.setData(Uri.parse("bazaar://details?id=app.majalo"));
                                    intent1.setPackage("com.farsitel.bazaar");
                                    startActivity(intent1);
                                }else{
                                    Toast.makeText(getApplicationContext(),"شما کافه بازار را نصب نکرده اید." , Toast.LENGTH_LONG).show();
                                }
                                break;
                            case R.id.ads:
                                /*mTracker.send(new HitBuilders.EventBuilder()
                                        .setCategory("TopicOption")
                                        .setAction("ads")
                                        .build());*/

                                new AlertDialog.Builder(activity)
                                        //.setTitle("تبلیغات در مجلو")
                                        .setCustomTitle(((Application) activity.getApplication()).DialogTitle(activity,"تبلیغات در مجلو"))
                                        .setMessage("به زودی ...")
                                        //.setIcon(android.R.drawable.ic_dialog_info)
                                        .show();
                                break;
                            case R.id.setting_button:
                                View view = LayoutInflater.from(activity).inflate(R.layout.dialog_font_setting, null, false);

                                final TextView test_text = (TextView) view.findViewById(R.id.test_text);
                                test_text.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/IRANSansMobile.ttf"));
                                try {
                                    if (shared_db.contains("settings_pageview_font_name")) {
                                        test_text.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/" + shared_db.getString("settings_pageview_font_name", "IRANSansMobile.ttf")));
                                    }
                                } catch (RuntimeException e) {}

                                test_text.setTextSize(16);
                                if (shared_db.contains("settings_pageview_font_size")) {
                                    test_text.setTextSize(shared_db.getInt("settings_pageview_font_size", 16));
                                }

                                final NumberPicker np = (NumberPicker) view.findViewById(R.id.font_size);
                                np.setMaxValue(30);
                                np.setMinValue(10);
                                np.setValue(16);
                                if (shared_db.contains("settings_pageview_font_size")) {
                                    np.setValue(shared_db.getInt("settings_pageview_font_size",16));
                                }
                                np.setWrapSelectorWheel(false);
                                np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                                    @Override
                                    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                                        test_text.setTextSize(newVal);
                                    }
                                });

                                final RadioGroup fn = (RadioGroup) view.findViewById(R.id.font_name);
                                fn.check(R.id.iransans);
                                fn.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                    @Override
                                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                                        try {
                                            switch (fn.getCheckedRadioButtonId()) {
                                                case R.id.iransans:
                                                    test_text.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/IRANSansMobile.ttf"));
                                                    break;
                                                case R.id.nazanin:
                                                    test_text.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/b_nazanin.ttf"));
                                                    break;
                                                case R.id.zar:
                                                    test_text.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/b_zar.ttf"));
                                                    break;
                                                case R.id.yekan:
                                                    test_text.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/b_yekan.ttf"));
                                                    break;
                                                case R.id.dastnevis:
                                                    test_text.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/danstevis.otf"));
                                                    break;
                                                default:
                                                    test_text.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/IRANSansMobile.ttf"));
                                                    break;
                                            }
                                        } catch (RuntimeException e) {}
                                    }
                                });
                                if (shared_db.contains("settings_pageview_font_name")) {
                                    String name = shared_db.getString("settings_pageview_font_name","IRANSansMobile.ttf");
                                    if (name.equals("IRANSansMobile.ttf"))
                                        fn.check(R.id.iransans);
                                    else if (name.equals("b_nazanin.ttf"))
                                        fn.check(R.id.nazanin);
                                    else if (name.equals("b_zar.ttf"))
                                        fn.check(R.id.zar);
                                    else if (name.equals("b_yekan.ttf"))
                                        fn.check(R.id.yekan);
                                    else if (name.equals("danstevis.otf"))
                                        fn.check(R.id.dastnevis);
                                }

                                AlertDialog.Builder builder =  new AlertDialog.Builder(activity);
                                builder.setView(view);
                                //builder.setTitle("تنظیمات نمایش");
                                builder.setCustomTitle(((Application) getApplication()).DialogTitle(activity,"تنظیمات نمایش"));
                                builder.setPositiveButton("ثبت", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (np != null)
                                            db_editor.putInt("settings_pageview_font_size",np.getValue()).commit();

                                        switch (fn.getCheckedRadioButtonId()) {
                                            case R.id.iransans:
                                                db_editor.putString("settings_pageview_font_name","IRANSansMobile.ttf").commit();
                                                break;
                                            case R.id.nazanin:
                                                db_editor.putString("settings_pageview_font_name","b_nazanin.ttf").commit();
                                                break;
                                            case R.id.zar:
                                                db_editor.putString("settings_pageview_font_name","b_zar.ttf").commit();
                                                break;
                                            case R.id.yekan:
                                                db_editor.putString("settings_pageview_font_name","b_yekan.ttf").commit();
                                                break;
                                            case R.id.dastnevis:
                                                db_editor.putString("settings_pageview_font_name","danstevis.otf").commit();
                                                break;
                                            default:
                                                db_editor.putString("settings_pageview_font_name","IRANSansMobile.ttf").commit();
                                                break;
                                        }

                                        return;
                                    } });
                                builder.setNegativeButton("انصراف", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        return;
                                    } });
                                AlertDialog dialog = builder.create();
                                dialog.show();

                                break;
                        }
                        menuItem.setChecked(false);
                        return false;
                    }
                });

        activity = this;

        mTracker = ((Application) activity.getApplication()).getDefaultTracker();
        shared_db = ((Application) activity.getApplication()).DB();
        db_editor = ((Application) activity.getApplication()).DB_editor();

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        _sGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(_sGridLayoutManager);

        List<Topic> topicList = new ArrayList<Topic>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_TOPICS + " order by position asc";

        DatabaseHandler databaseHandler = new DatabaseHandler(this);
        db = databaseHandler.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Topic topic = new Topic();
                topic.setId(Integer.parseInt(cursor.getString(0)));
                topic.setTitle(cursor.getString(1));
                topic.setOrder(Integer.parseInt(cursor.getString(2)));
                topic.setThumb_url(cursor.getString(3));

                topicList.add(topic);
                topics.put(String.valueOf(topic.getId()),topic);

            } while (cursor.moveToNext());
        }

        MainActivityRecyclerViewAdapter rcAdapter = new MainActivityRecyclerViewAdapter(MainActivity.this, topicList);
        recyclerView.setAdapter(rcAdapter);
    }

    public static void setBadge(Context context, int count) {
        String launcherClassName = getLauncherClassName(context);
        if (launcherClassName == null) {
            return;
        }
        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        intent.putExtra("badge_count", count);
        intent.putExtra("badge_count_package_name", context.getPackageName());
        intent.putExtra("badge_count_class_name", launcherClassName);
        context.sendBroadcast(intent);
    }

    public static String getLauncherClassName(Context context) {
        PackageManager pm = context.getPackageManager();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resolveInfos) {
            String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
            if (pkgName.equalsIgnoreCase(context.getPackageName())) {
                String className = resolveInfo.activityInfo.name;
                return className;
            }
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            new AlertDialog.Builder(this)
                    .setCustomTitle(((Application) getApplication()).DialogTitle(this, "خروج"))
                    .setMessage("آیا میخواهید از برنامه خارج شوید؟")
                    .setPositiveButton("بلی", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            MainActivity.super.onBackPressed();
                        }
                    })
                    .setNegativeButton("خیر", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    //.setIcon(android.R.drawable.ic_dialog_info)
                    .show();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume(){
        super.onResume();
        mTracker.setScreenName("Dashboard Screen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        setBadge(activity,0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.main2, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    private boolean isPackageInstalled(String packagename, PackageManager pm) {
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }
}
